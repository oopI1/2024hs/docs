= Übungsaufgaben
:stem: latexmath
:icons: font
:hide-uri-scheme:
:sectnums:


== `Wochentag` (Vorlesung)

Im Package `wochentage` in Ihrer Vorlage finden Sie ein Programm `WochentageHerkunft`. Studieren Sie es, bis Sie die wichtigsten Aspekte verstehen, und probieren Sie es aus.

[loweralpha]
. Das Kernstück des Programms ist die Methode `herkunft`, welche bisher mit Strings als Wochentage arbeitet. Das wollen wir ändern. Erstellen Sie dazu ein Enum `Wochentag`.

. Ändern Sie die Methode `herkunft` so ab, dass statt einem String der Typ `Wochentag` als Parameter verwendet wird, und passen Sie die `main`-Methode entsprechend an.
+
*Tipp:* Verwenden Sie die `valueOf`-Methode zum Umwandeln eines Strings in ein Enum-Objekt.

.*Lösungen*
[%collapsible]
====
[loweralpha]
. {empty}
+
[source,java]
----
package wochentage;

public enum Wochentag {
    MO, DI, MI, DO, FR, SA, SO
}
----

. {empty}
+
[source,java]
----
package wochentage;

import java.util.Objects;
import java.util.Scanner;

public class WochentageHerkunft {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Geben Sie einen Wochentag ein: ");
        String line = scanner.nextLine().toUpperCase();
        while (line.length() >= 2) {
            Wochentag tag = Wochentag.valueOf(line.substring(0, 2));
            String herkunft = herkunft(tag);
            System.out.println(herkunft + "\n");

            System.out.print("Geben Sie nochmals einen Wochentag ein: ");
            line = scanner.nextLine().toUpperCase();
        }
    }

    private static String herkunft(Wochentag tag) {
        if (tag == Wochentag.MO) {
            return "«Montag» kommt von der römischen Mondgöttin, Luna.";
        } else if (tag == Wochentag.DI) {
            return "«Dienstag» kommt vom nordisch-germanischen Gott Tyr.";
        } else if (tag == Wochentag.MI) {
            return "«Mittwoch» kommt davon, dass dieser Tag nach\n" +
                   "mittelalterlicher Zählweise in der Mitte der Woche lag.";
        } else if (tag == Wochentag.DO) {
            return "«Donnerstag» kommt vom nordisch-germanischen Gott Donar (Thor).";
        } else if (tag == Wochentag.FR) {
            return "«Freitag» kommt von nordisch-germanischen Göttin Frija.";
        } else if (tag == Wochentag.SA) {
            return "«Samstag» kommt von althochdeutsch «sambaztac», das auf\n" +
                   "den römischen Gott Saturn zurückgeht.";
        } else {
            return "«Sonntag» kommt vom römischen Sonnengott, Sol.";
        }
    }
}
----
====


== `Month` (Vorlesung)

[loweralpha]
. Erstellen Sie ein Package `months` und darin eine Enum `Month` für die zwölf Monate des Jahres (mit englischen Namen). Testen Sie Ihre Lösung, indem Sie die ersten beiden Tests in `MonthTest` einkommentieren und ausführen.

. Fügen Sie eine Methode `days` hinzu, welche die Anzahl Tage in diesem Monat zurückgibt. Da dies beim Februar davon abhängig ist, ob es um ein Schaltjahr geht, fügen Sie einen Parameter `leapYear` hinzu. Prüfen Sie mit dem nächsten Unit-Test.

. Überschreiben Sie die `toString`-Methode, sodass der Name des Monats mit herkömmlicher Gross-/Kleinschreibung zurückgegeben wird, z. B. `"January"`. Siehe letzter Unit-Test.

.*Lösung*
[%collapsible]
====
[source,java]
----
package months;

public enum Month {
    JANUARY, FEBRUARY, MARCH, APRIL, MAY, JUNE,
    JULY, AUGUST, SEPTEMBER, OCTOBER, NOVEMBER, DECEMBER;

    public int days(boolean leapYear) {
        if (this == FEBRUARY && !leapYear) {
            return 28;
        } else if (this == FEBRUARY) {
            return 29;
        } else if (this == APRIL || this == JUNE ||
                   this == SEPTEMBER || this == NOVEMBER) {
            return 30;
        } else {
            return 31;
        }
    }

    public String toString() {
        return name().charAt(0) + name().substring(1).toLowerCase();
    }
}
----
====

== `herkunft` mit `switch` (Vorlesung)

Schreiben Sie die Methode `herkunft` im Programm `WochentageHerkunft` nochmals um, indem Sie das `if`/`else`-Konstrukt mit einem `switch`-Ausdruck ersetzen.

Überprüfen Sie kurz, dass das Weglassen eines Falls ein Compile-Fehler erzeugt.

.*Lösung*
[%collapsible]
====
[source,java]
----
private static String herkunft(Wochentag tag) {
    return switch (tag) {
        case MO -> "«Montag» kommt von der römischen Mondgöttin, Luna.";
        case DI -> "«Dienstag» kommt vom nordisch-germanischen Gott Tyr.";
        case MI -> "«Mittwoch» kommt davon, dass dieser Tag nach\n" +
                   "mittelalterlicher Zählweise in der Mitte der Woche lag.";
        case DO -> "«Donnerstag» kommt vom nordisch-germanischen Gott Donar (Thor).";
        case FR -> "«Freitag» kommt von nordisch-germanischen Göttin Frija.";
        case SA -> "«Samstag» kommt von althochdeutsch «sambaztac», das auf\n" +
                   "den römischen Gott Saturn zurückgeht.";
        case SO -> "«Sonntag» kommt vom römischen Sonnengott, Sol.";
    };
}
----
====


== `switch`-Regeln (Vorlesung)

Gegeben sei folgende Enum:

[source,java]
----
public enum Priority {
    LOW, MEDIUM, HIGH
}
----

Welche der folgenden `switch`-Verzweigungen sind gültig?

[loweralpha]
. {empty}
+
[source,java]
----
Priority p = ...
String german = switch (p) {
    case LOW -> System.out.println("niedrig");
    case MEDIUM -> System.out.println("mittel");
    case HIGH -> System.out.println("hoch");
};
----

. {empty}
+
[source,java]
----
Priority p = ...
String s = switch (p) {
    default -> "to do";
}
----

. {empty}
+
[source,java]
----
Priority p = ...
switch (p) {
    case MEDIUM -> doItLater();
    case HIGH -> doItRightNow();
}
----

. {empty}
+
[source,java]
----
Priority p = ...
int bounty = switch (p) {
    default -> 0;
    case MEDIUM -> 500;
    case HIGH -> 1000;
};
----

. {empty}
+
[source,java]
----
int bounty = ...
Priority p = switch (bounty) {
    case 0 -> Priority.LOW;
    case 500 -> Priority.MEDIUM;
    case 1000 -> Priority.HIGH;
};
----

.*Lösungen*
[%collapsible]
====
[loweralpha]
. Ungültig: `switch` steht auf der rechten Seite einer Zuweisung und ist damit ein _Ausdruck_. Deshalb muss es ein Resultat produzieren. Die Codestücke für die einzelnen Fälle produzieren aber kein Resultat, sondern geben etwas auf der Konsole aus.

. Ungültig, aber nur, weil am Ende ein `;` fehlt. Da das `switch` auf der rechten Seite einer Zuweisung steht, ist es ein Ausdruck und damit Teil einer normalen Anweisung, die mit `;` abgeschlossen werden muss.

. Gültig: Dieses `switch` ist eine Anweisung (kein Ausdruck) und muss deshalb nicht alle Fälle des Enums abdecken. Für den Fall `LOW` wird einfach nichts gemacht.

. Gültig: Statt alle Enum-Objekte abzudecken, darf man auch bei einem Enum ein `default`-Fall angeben. Es spielt keine Rolle, wo dieser Fall steht.

. Ungültig: Dieses `switch` ist ein Ausdruck und muss deshalb vollständig sein. Hier wird allerdings nicht über Enum-Objekte ge``switch``t, sondern über `int`-Werte. Deshalb muss es einen `default`-Fall geben.
====


== `switch`-Ausdrücke vs. -Anweisungen

[loweralpha]
. Schreiben Sie folgenden `switch`-Ausdruck in eine ``switch``_-Anweisung_ um, also um ein `switch`, das nicht auf der rechten Seite einer Zuweisung steht, sondern (ähnlich wie `if`/`else`) als eigenständige Anweisung verwendet wird.
+
[source,java]
----
Wochentag t = ...
String name = switch (t) {
    case MO -> "Montag";
    case DI -> "Dienstag";
    case MI -> "Mittwoch";
    case DO -> "Donnerstag";
    case FR -> "Freitag";
    case SA -> "Samstag";
    case SO -> "Sonntag";
};
System.out.println(name);
----

. Machen Sie dasselbe für folgende _beiden_ `switch`-Ausdrücke, sodass nur noch _eine_ `switch`-Anweisung verwendet wird:
+
[source,java]
----
Wochentag t = ...
String kategorie = switch (t) {
    case MO, DI, MI, DO, FR -> "Wochentag";
    case SA, SO -> "Wochenende";
};
double ansatz = switch (t) {
    case MO, DI, MI, DO, FR -> 1.0;
    case SA, SO -> 1.5;
};
// 'kategorie' und 'ansatz' werden weiterverwendet
----

.*Lösungen*
[%collapsible]
====
[loweralpha]
. {empty}
+
[source,java]
----
Wochentag t = ...
String name;
switch (t) {
    case MO -> name = "Montag";
    case DI -> name = "Dienstag";
    case MI -> name = "Mittwoch";
    case DO -> name = "Donnerstag";
    case FR -> name = "Freitag";
    case SA -> name = "Samstag";
    case SO -> name = "Sonntag";
}
System.out.println(name);
----
+
oder kürzer (falls `name` weiter unten nicht mehr verwendet wird):
+
[source,java]
----
Wochentag t = ...
switch (t) {
    case MO -> System.out.println("Montag");
    case DI -> System.out.println("Dienstag");
    case MI -> System.out.println("Mittwoch");
    case DO -> System.out.println("Donnerstag");
    case FR -> System.out.println("Freitag");
    case SA -> System.out.println("Samstag");
    case SO -> System.out.println("Sonntag");
}
----

. {empty}
+
[source,java]
----
Wochentag t = ...
String kategorie;
double ansatz;
switch (t) {
    case MO, DI, MI, DO, FR -> {
        kategorie = "Wochentag";
        ansatz = 1.0;
    }
    case SA, SO -> {
        kategorie = "Wochenende";
        ansatz = 1.5;
    }
}
----
+
Beachten Sie, dass es hier die `{}` braucht, da mehrere Anweisungen pro Fall ausgeführt werden.
====


== Methode mit Switch

`switch`-Ausdrücke können auch in Methoden zum Auswählen des Rückgabewerts verwendet werden.

[loweralpha]
. Ergänzen Sie den folgenden Code mit der korrekten Berechnung der enthaltenen Mehrwertsteuer:
+
[source, java]
----
public class MwSt {
    // 0: Normaltarif von 8.1%
    // 1: reduzierter Satz von 2.6%
    // 2: Sondersatz Beherbergung von 3.8%

    public static void main(String[] args) {
        double enthalteneMwSt= getMwSt(200, 0);
    }

    public static double getMwSt(double betrag, int category){
        return switch(category){
            default -> ...;
            case 1 -> ...;
            case 2 -> ...;
        };
    }
}
----
+
Ändern Sie nun noch `default` zu `case 0` ab. Welche Meldung erhalten Sie? Wie können Sie sich diese erklären?

. Ersetzen Sie nun die Zahlen 0, 1 und 2 durch Objekte einer neue Enum `MwStKategorie`.


== Enumeration `Kantone`
Scheiben Sie eine Enumeration für die Schweizer Kantone. Die Kantone sollen die Attribute
`einwohnerzahl` (mit Getter und Setter), `flaeche` (nur Getter) sowie `sprache` (nur Getter) haben.
Sie können sich natürlich auch auf ihre Lieblingskantone beschränken.


== Enumeration `UserRole`

Bei vielen Programmen gibt es unterschiedliche User-Rollen. In dieser Aufgabe schreiben Sie dafür eine Enumeration `UserRole`. Jede der Rollen hat eine zugewiesene Schreib- und Leseberechtigung. Die Rollen sind wie folgt definiert:

[%autowidth,frame=ends,grid=rows]
|===
| Rolle | Schreibberechtigung | Leseberechtigung

| `ADMIN` | `ALL` | `ALL`
| `POWERUSER` | `UNCRITICAL` | `ALL`
| `USER` | `UNCRITICAL` | `UNCRITICAL`
| `VISITOR` | `NONE` | `UNCRITICAL`
|===

Dazu brauchen wir gleich noch eine Enumeration `Authorization` für die Berechtigungen, mit den Objekten `ALL`, `UNCRITICAL` und `NONE`.

[loweralpha]
. Schreiben Sie die Enumeration `Authorization` mit den Werten `ALL`, `UNCRITICAL` und `NONE`.

. Schreiben Sie die Enumeration `UserRole`. Diese hat die vie oben aufgeführten Werte und jedem Wert ist eine Schreibberechtigung (`writePermission`) und eine Leseberechtigung (`readPermission`) zugewiesen. Die Schreib und Leseberechtigung sollen abgefragt werden dürfen, aber nicht verändert werden (Stichwort `final`).