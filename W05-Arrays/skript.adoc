= Arrays
:sectnums:
:appendix-caption: Anhang

Zum Speichern von Daten stehen nicht nur die primitiven Datentypen zur Verfügung, sondern oft finden ganze Datenstrukturen ihre Anwendung. Eine davon ist die Klasse Array. Ihre ersten Arrays haben sie gesehen, wenn sie Parameter der main-Methode geschrieben haben (String[] args).

== Deklaration
Wie bei den anderen Variablen, müssen wir dem Compiler sagen, was für die Daten wir speichern möchten.

Syntax

`_Datentyp_[] variablenname;`

`_Datentyp_ variablenname[];`

Beispiele
[source, java]
int[] zahlen1;
int zahlen2[];
int[][] matrix1;
int matrix2[][];

== Arrays erzeugen
Nach der Deklaration ist nun aber noch nicht klar, wie viele Elemente gespeichert werden sollen. Dies geschieht mit dem new Operator, den wir dann bei den Objekten noch besser kennen lernen werden.

Syntax:

`name = new _Datentyp_[anzahl];`

Beispiele:
[source, java]
zahlen= new int[6];
matrix = new int[3][4];

Wenn zum Zeitpunkt der Deklaration bereits bekannt ist, wie viele Elemente benötigt werden (das kann auch in Form einer Variablen sein), dann kann man die Grösse gleich angegeben werden, oder man kann gleich die Elemente definieren (Zeile 3).

Beispiele
[source, java, linenum]
int[] zahlen= new int[6];
double[][] matrix = new double[3][4];
int[] reihe= {1,2,3,4,5,6,7,8,9};

== Wertzuweisung
Einer Zelle (oder auch Feld) kann ein Wert zugewiesen werden, wenn der Platz im Array angeben wird. Bei eindimensionalen Arrays ist das ein Wert (fixe Zahl oder int-Variable). Bei mehrdimensionalen Arrays sind es entsprechend mehrere Indizes, die angegeben werden müssen.

[source, java]
zahlen[0]= 1;   //weist dem ersten Element den Wert 1 zu.
zahlen[1]= 2;	//weist dem zweiten Element 2 zu.
matrix[2][1]= 5;

Java beginnt jeweils mit dem Index 0 und endet mit dem Index (Länge -1). Ein Array int[5] hat also die Elemente mit den Indizes 0,1,2,3 und 4.

== Länge eines Arrays
Bei einem Array kann mit einem Befehl `length`, die Länge des Arrays abgefragt werden. Das ist vor allem beim Durchlaufen des Arrays hilfreich.

Beispiel
[source, java]
int[] zahlen= new int[6];
for ( int i=0; i <zahlen.length; i++){
	zahlen[i]= i;
}


Bei mehrdimensionalen Arrays funktioniert dies natürlich auch. Wichtig ist hier zu verstehen, dass ein zweidimensionales Array ein eindimensionales Array ist, in dem in den einzelnen "Zellen" wieder eindimensionale Arrays sind.

[source, java]
int[][] ab= new int[9][6];
for (int i=0; i<9; i++){
    for (int j=0; j<6; j++){
        int k= ab[i][j];
        System.out.println(k);
    }
}
for (int i=0; i<ab.length; i++){
    for (int j=0; j<ab[i].length; j++){
        int k= ab[i][j];
        System.out.println(k);
    }
}

== Durchlaufen eines Arrays: Kurzschreibweise zum Auslesen der Elemente
Für das Ausgeben, oder auslesen der Werte in einem Array gibt es eine Kurzschreibweise. Diese kann aber nicht verwendet werden, wenn Elemente im Array gespeichert werden sollen!

Beispiel:
[source, java]
----
for (int j=0; j<9; j++){
    int k= a[j];
    System.out.println(k);
}
// kurz
for (int k: a){
    System.out.println(k);
}
// Zweidimensional
for (int i=0; i<ab.length; i++){
    for (int j=0; j<ab[i].length; j++){
        int k= ab[i][j];
        System.out.println(k);
    }
}
// kurz
for (int[] row:ab){
    for (int k:row){
        System.out.println(k);
    }
}
----

== Zeichenketten (Strings) als Arrays

Ein String ist eine Zusammenfassung von mehreren Variablen des Typs Character (char). In Java wird eine Zeichenkette als Array des Datentyps char angelegt. Ein String kann somit auch aus einem char-Array erzeugt werden. Auf die einzelnen Buchstaben im String kann wie beim Array über den Index zugegriffen werden.

Beispiel:
[source, java]
char[] meinText = {'d','e','r',' ','T','e','x','t'};
String meinString = new String(meinText);
char erstesZeichen= meinString.charAt(0);

Bei diesem Beispiel wird als erstes ein char-Array der Länge 8 erzeugt und direkt mit 8 Zeichen initialisiert. Auf Basis dieses Arrays wird dann ein String erzeugt, der den Text "der Text" enthält. Aus diesem String wird anschliessend das erste Zeichen ausgelesen. Will man alle Zeichen eines Textes auf diese Weise auslesen sieht das wie folgt aus:

[source, java]
String meinString = "Text";
for (int i=0; i<meinString.length(); i++){
	System.out.print(meinString.charAt(i));
}

Man kann alternativ auch ein Array erstellen lassen und dann damit wie gewohnt weiterarbeiten.
[source, java]
char[] buchstaben= meinString.toCharArray();
for (int i=0; i<buchstaben.length; i++){
    System.out.print(buchstaben[i]);
}
