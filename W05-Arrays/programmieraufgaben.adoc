= Programmieraufgaben
:stem: latexmath
:icons: font
:sectnums:

== Höhenprofil zeichnen

Vervollständigen Sie das Programm `Hoehenprofil` in Ihrem Repository, sodass es das Höhenprofil einer Wanderung von Basel nach Brugg zeichnet:

[.text-center]
image:res/hoehenprofil.png[width=400]

Dafür sind zwei Arrays im Programm vorgegeben, eines mit den Höhenangaben für die Wegpunkte (in m ü. M.) und eines mit den Distanzen zwischen den Wegpunkten (in km). Beachten Sie, dass das zweite Array einen Wert weniger enthält! Ebenfalls bereits vorgegeben sind zwei Variablen `pxProHm` und `pxProKm`, welche die Skalierung der Zeichnung definieren.

**Zusatzaufgabe:** Wie im Bild oben gezeigt, können Sie eine einfache (wenn auch nicht so hübsche) Darstellung implementieren, bei der zwei Punkte im Diagramm mit zwei Linien und rechten Winkel verbunden werden. Als (mathematische) Herausforderung können Sie auch versuchen, direkte Linien zwischen den Punkten zu zeichnen, was zu einer glatteren Darstellung führt:

[.text-center]
image:res/hoehenprofil-2.png[width=400]


== Temperatur-Analyse

IMPORTANT: Das ist eine obligatorische Aufgabe und _muss eigenhändig und alleine gelöst werden._

In Ihrer Vorlage finden Sie eine Klasse `Temperatur`, welche fünf Methoden enthält, die Arrays mit Temperaturdaten analysieren. Lesen Sie die Methodenkommentare und implementieren Sie die Methoden entsprechend. Beachten Sie die Unit-Tests in der Klasse `TemperaturTest`, welche Informationen zu Spezialfällen enthalten.


== Random-Sort

IMPORTANT: Das ist eine obligatorische Aufgabe und _muss eigenhändig und alleine gelöst werden._

In dieser Aufgabe implementieren Sie einen nicht besonders effizienten, aber dafür witzigen Sortieralgorithmus für Arrays, _Random-Sort_. Dieser Algorithmus wählt wiederholt zwei zufällige Indizes im Array und vertauscht die beiden Werte, _falls sie nicht schon in der richtigen Reihenfolge sind_ (nur relativ zueinander). Das wird so lange wiederholt, bis das Array vollständig sortiert ist.

.*Beispiel*
[%collapsible]
====
Für das Array `[3, 4, 2, 1]` könnte der Algorithmus per Zufall folgende Schritte ausführen:

. Wähle Indizes 1 und 3: `[3, *4*, 2, *1*]`. Die Werte 4 und 1 haben die falsche Reihenfolge, also werden sie getauscht:
+
`[3, *1*, 2, *4*]`

. Wähle Indizes 0 und 3: `[*3*, 1, 2, *4*]`. Die Werte 3 und 4 sind schon in der richtigen Reihenfolge, also passiert nichts.

. Wähle Indizes 0 und 2: `[*3*, 1, *2*, 4]`. Die Werte 3 und 2 haben die falsche Reihenfolge, also werden sie getauscht:
+
`[*2*, 1, *3*, 4]`

. Wähle Indizes 1 und 2: `[2, *1*, *3*, 4]`. Die Werte 1 und 3 haben bereits die richtige Reihenfolge, also passiert nichts.

. Wähle Indizes 0 und 1: `[*2*, *1*, 3, 4]`. Die Werte 2 und 1 haben die falsche Reihenfolge, also werden sie getauscht:
+
`[*1*, *2*, 3, 4]`

Damit ist das Array sortiert und der Algorithmus terminiert.
====

Gehen Sie wie folgt vor:

[loweralpha]
. Implementieren Sie als Erstes die Methode `isSorted` in der Klasse `RandomSort`. Diese Methode prüft, ob ein `int`-Array richtig sortiert ist (aufsteigend – von klein nach gross) und gibt entsprechend `true` oder `false` zurück. Die Methode dient als Grundlage für den zweiten Schritt unten. Beachten Sie die Unit-Tests in der Klasse `RandomSortTest`.
. Jetzt implementieren Sie noch die Methode `randomSort`, welche eine Referenz zu einem Array erhält und dieses nach dem oben beschriebenen Algorithmus sortieren soll. Als Rückgabewert soll die Methode die Anzahl getätigter Tauschoperationen zurückgeben. Da der Algorithmus auf Zufall basiert, kann diese Anzahl variieren, aber nicht beliebig. Wenn z. B. ein Array mit folgenden Zahlen sortiert wird: `[1, 2, 3, 6, 5, 4]`, gibt es nur zwei Möglichkeiten, entweder passiert eine einzige Tauschoperation (6 ↔ 4), oder drei (6 ↔ 5, 6 ↔ 4, 5 ↔ 4); alles andere ist unmöglich.
+
Dieser und weitere Test-Inputs sind auch als Unit-Tests vorhanden.
