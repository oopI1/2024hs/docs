= Übungsaufgaben
:sectnums:
:stem: latexmath


== `if`/`else` lesen (Vorlesung)

Gegeben sei folgendes Codestück:

[source,java]
----
if (x > 0) {
    System.out.println("positiv");
}
if (x > y) {
    System.out.println(x - y);
} else {
    System.out.println(y - x);
    if (x == y) {
        System.out.println("gleich");
    }
}
----

Welche Ausgabe wird erzeugt, wenn `x` und `y` wie folgt initialisiert werden?

[loweralpha]
. `int x = 6, y = 11;`
. `int x = 0, y = -42;`
. `int x = -7, y = 2;`
. `int x = 4, y = 4;`

.*Lösungen*
[%collapsible]
====
[loweralpha]
. {empty}
+
----
positiv
5
----
. {empty}
+
----
42
----
. {empty}
+
----
9
----
. {empty}
+
----
positiv
0
gleich
----
====


== `if`-Anweisungen schreiben

Übersetzen Sie jede der folgenden Aussagen in eine `if`-Anweisung. Nehmen Sie an, es gäbe `int`-Variablen `x`, `y`, und `z`.

*Beispiel:* y ist positiv. -> *Lösung:* `if (y > 0) { ... }`

[loweralpha]
. `x` entspricht der Summe von `y` und `z`
. `z` ist nicht Null
. `z` ist ungerade (d. h. ohne Rest durch 2 teilbar)
. Das Quadrat von `z` ist nicht kleiner als `y`
. `y` ist ein Vielfaches von `z`
. `y` ist eine nicht-negative einstellige Zahl
. `x` und z haben unterschiedliche Vorzeichen (`+` oder `-`)
. Entweder ist `x` oder `y` gerade, aber nicht beide

.*Lösungen*
[%collapsible]
====
[loweralpha]
. `if (x == y + z) { ... }`
. `if (z != 0) { ... }`
. `if (z % 2 == 1) { ... }`
. `if (z * z >= y) { ... }`
. `if (z % y == 0) { ... }`
. `if (y >= 0 && y < 10) { ... }`
. `if (x > 0 && y < 0 || x < 0 && y > 0) { ... }`
. `if \((x % 2 == 1 || y % 2 == 1) && !(x % 2 == 1 && y % 2 == 1)) { ... }`
====


== Rätselhafte `if`/`else`

Gegeben sei folgendes Codestück:

[source,java]
----
int z = 4;
if (z <= x) {
    z = x + 1;
} else {
    z += 9;
}
if (z <= y) {
    y++;
}
System.out.println(z + " " + y);
----

Welche Ausgabe der Code, wenn `x` und `y` wie folgt initialisiert werden?

[loweralpha]
. `int x = 3, y = 20;`
. `int x = 4, y = 5;`
. `int x = 5, y = 5;`
. `int x = 6, y = 10;`

.*Lösungen*
[%collapsible]
====
[loweralpha]
. `13 21`
. `5 6`
. `6 5`
. `7 11`
====


== Problem mit `if`

Was ist das Problem mit folgendem Code? Beheben Sie es.

[source,java]
----
Scanner scanner = new Scanner(System.in);
System.out.println("Was ist Ihre Lieblingsfarbe?");
String farbe = scanner.next();
if (farbe == "blue") {
    System.out.println("Meine auch!");
}
----

.*Lösung*
[%collapsible]
====
Der Code vergleicht die Strings mit `==`, was nicht funktioniert. Strings müssen mit der Methode `equals` verglichen werden. Der Code sollte also so aussehen:

[source,java]
----
Scanner scanner = new Scanner(System.in);
System.out.println("Was ist Ihre Lieblingsfarbe?");
String farbe = scanner.next();
if (farbe.equals("blue")) {
    System.out.println("Meine auch!");
}
----
====


== `if`/`else` einsetzen

Stellen Sie aus folgenden Codezeilen ein Programm zusammen, welches eine Zahl einliest und ausgibt, ob sie positiv, negativ oder 0 ist. Lassen Sie sich nicht von den fehlerhaften Zeilen irritieren.😉

++++
<script>
    parsonsProblem`
        import java.util.Scanner;
        public class Signum {
            public static void main(String[] args) {
                Scanner scanner = new Scanner(System.in);
                int zahl = scanner.nextInt();
                if (zahl > 0) {
                    System.out.println("positiv");
                } else if (zahl < 0) {
                    System.out.println("negativ");
                } else {
                    System.out.println("0");
                }
            }
        }
        if (zahl >= 0) { #distractor
        if zahl > 0 { #distractor
        endif #distractor
        } else (zahl == 0) { #distractor
    `;
</script>
++++


== Redundanz mit `if`/`else`

Der folgende Code enthält _Redundanz_, d. h. es kommen Codestücke doppelt vor. Schreiben Sie den Code so um, dass kein doppelter und kein unnötiger Code vorkommt:

[source,java]
----
if (x < 30) {
    a = 2;
    x++;
    System.out.println("Java is awesome! " + x);
} else {
    a = 2;
    System.out.println("Java is awesome! " + x);
}
----

.*Lösung*
[%collapsible]
====
[source,java]
----
a = 2;
if (x < 30) {
    x++;
}
System.out.println("Java is awesome! " + x);
----
====


== Fehler finden

Das folgende Codestück soll die Summe der Zahlen von 1 bis und mit `n` berechnen, es haben sich aber zwei Fehler und eine Unschönheit eingeschlichen. Finden und beheben Sie sie.

[source,java]
----
for (int i = 0; i < n; i++) {
    int summe = 0;
    summe += i;
}
System.out.println(summe);
----

.*Lösung*
[%collapsible]
====
. Der Test in der `for`-Schleife sollte `i \<= n` sein, damit die Zahl `n` auch noch addiert wird.
. Die Deklaration und Initialisierung der `summe`-Variable sollte aus der Schleife herausgenommen werden, ansonsten wird sie bei jeder Iteration auf `0` zurückgesetzt. Ausserdem ist die Variable so ausserhalb der Schleife gar nicht sichtbar, die letzte Zeile würde also nicht kompilieren. Mehr dazu nächste Woche.
. `i` sollte besser bei `1` statt `0` starten; das ist zwar kein Fehler, denn die Summe ist auch so korrekt. Aber es ist unschön, da die Schleife eine unnötige Iteration durchführt.

Die korrigierte Version sieht so aus:

[source,java]
----
int summe = 0;
for (int i = 1; i <= n; i++) {
    summe += i;
}
System.out.println(summe);
----
====


== Min/Max einer Sequenz (Vorlesung)

Vervollständigen Sie folgendes Programm so, dass es die grösste der 10
eingegebenen Zahlen findet und ausgibt:

[source,java]
----
Scanner s = new Scanner(System.in);
System.out.print("Gib 10 Zahlen ein: ");

// TODO

System.out.println("Grösste: " + /* TODO */);
----

Schreiben Sie zuerst einen Lösungsansatz in Pseudocode auf.

.*Lösung*
[%collapsible]
====
[source,java]
----
Scanner s = new Scanner(System.in);
System.out.print("Gib 10 Zahlen ein: ");

int max = s.nextInt();
for (int i = 1; i <= 9; i++) {
    int next = s.nextInt();
    if (next > max) {
        max = next;
    }
}

System.out.println("Grösste: " + max);
----
====


== `while`-Schleifen verfolgen (zum Teil Vorlesung)

Geben Sie für jede der folgenden Schleifen an, wie oft der Body ausgeführt wird (möglicherweise auch 0× oder unendlich mal) und wie die Ausgabe lautet.

Wenn nötig, schreiben Sie eine kleine Tabelle mit den Variableninhalten auf, wie letzte Woche besprochen.

[loweralpha]
. {empty}
+
[source,java]
----
int x = 1;
while (x <= 100) {
    System.out.print(x + " ");
    x += 10;
}
----

. {empty}
+
[source,java]
----
int max = 10;
while (max < 10) {
    System.out.print(max + " ");
    max--;
}
----

. {empty}
+
[source,java]
----
int x = 250;
while (x % 3 != 0) {
    System.out.print(x + " ");
}
----

. {empty}
+
[source,java]
----
int x = 2;
while (x < 200) {
    System.out.print(x + " ");
    x *= x;
}
----

. {empty}
+
[source,java]
----
String word = "a";
while (word.length() < 10) {
    word = "b" + word + "b";
}
System.out.println(word);
----

. {empty}
+
[source,java]
----
int x = 100;
while (x > 0) {
    System.out.print(x + " ");
    x = x / 2;
}
----

.*Lösungen*
[%collapsible]
====
[loweralpha]
. 10×, Ausgabe: `1 11 21 31 41 51 61 71 81 91 `
. 0×, keine Ausgabe
. Endlosschleife, da `250 % 3 != 0` wahr ist und `x` nicht verändert wird; Ausgabe: `250 250 250 ...`
. 3×, Ausgabe: `2 4 16 `
. 5×, Ausgabe: `bbbbbabbbbb`
. 7×, Ausgabe: `100 50 25 12 6 3 1 `
====


== `while`-Schleife einsetzen

Stellen Sie aus folgenden Zeilen ein Codestück zusammen, welches eine Variable `x`, beginnend mit dem Wert 1, so lange verdoppelt, wie sie kleiner als 1'000'000 ist. Danach soll ausgegeben werden, wie oft `x` verdoppelt wurde.

*Beispiel:* Nach 4 Verdopplungen ist `x` = 2 × 2 × 2 × 2 = 16.

++++
<script>
    parsonsProblem`
        int i = 0;\\nint x = 1;
        while (2 * x < 1_000_000) {
            x *= 2;\\ni++;
        }
        System.out.println(i);
        while (i < 1_000_000) { #distractor
        while (x < 1_000_000) { #distractor
        if (x < 1_000_000) { #distractor
        x /= 2;\\ni--; #distractor
        System.out.println(x); #distractor
    `;
</script>
++++

*Zusatzaufgabe:* Schätzen Sie mal im Kopf ab, wie viele Verdoppelungen es sind.

.*Lösung*
[%collapsible]
====
Es sind nur 19 Verdoppelungen: 2 hoch 19 ist 524'288, 2 hoch 20 ist bereits 1'048'576. Als Faustregel kann man sich merken, dass 2 hoch 10 etwa 1'000 ist, 2 hoch 20 etwa 1'000'000, 2 hoch 30 etwa 1'000'000'000, usw.
====


== Zufallsgenerator

Verwenden Sie die `Random`-Klasse für folgende Aufgaben. Denken Sie an die benötigte `import`-Anweisung am Anfang des Programms:

[source,java]
----
import java.util.Random;
----

[loweralpha]
. Schreiben Sie ein Mini-Programm, welches einen zufälligen `int` zwischen –10 und +10 (inklusive) ausgibt.

. Schreiben Sie ein Programm, das einen zufälligen Grossbuchstaben des Alphabets ausgibt. Denken Sie daran, dass man mit `char`-Werten «rechnen» kann.

. Erweitern Sie das Programm so, dass es einen String mit einer zufälligen Länge zwischen 1 und 10 und lauter zufälligen Grossbuchstaben ausgibt.

.*Lösungen*
[%collapsible]
====
[loweralpha]
. {empty}
+
[source,java]
----
import java.util.Random;

public class RandomInt {
    public static void main(String[] args) {
        Random random = new Random();
        int zufall = random.nextInt(-10, 11);
        System.out.println(zufall);
    }
}
----

. {empty}
+
[source,java]
----
import java.util.Random;

public class RandomChar {
    public static void main(String[] args) {
        Random random = new Random();
        int zufall = random.nextInt(26); // oder nextInt(0, 26)
        char buchstabe = (char) ('A' + zufall);
        System.out.println(buchstabe);
    }
}
----

. {empty}
+
[source,java]
----
import java.util.Random;

public class RandomString {
    public static void main(String[] args) {
        Random random = new Random();
        int laenge = random.nextInt(1, 11);
        String s = "";
        for (int i = 1; i <= laenge; i++) {
            int zufall = random.nextInt(26);
            char buchstabe = (char) ('A' + zufall);
            s += buchstabe;
        }
        System.out.println(s);
    }
}
----
====


== Logische Operatoren (Vorlesung)

Gegeben seien folgende Variablen-Deklarationen:

[source,java]
----
int x = 27;
int y = -1;
int z = 32;
boolean b = false;
----

Welche Werte haben folgende Ausdrücke?

[loweralpha]
. `!b`
. `b || true`
. `x > y && y > z`
. `!(x % 2 == 0)`
. `b && !b`
. `b || !b`
. `!(x / 2 == 13.5) || b`
. `(z < x) == b`

.*Lösungen*
[%collapsible]
====
[loweralpha]
. `true`
. `true`
. `false`
. `true`
. `false` (egal, was `b` ist)
. `true` (egal, was `b` ist)
. `true`
. `true`
====


== `boolean` Zen

Verbessern Sie mittels «`boolean` Zen» folgendes Codestück:

[source,java]
----
boolean nurNoten = (betrag % 10 == 0);
boolean brauchtMuenzen;
if (nurNoten == false) {
    brauchtMuenzen = true;
} else {
    brauchtMuenzen = false;
}
System.out.println(brauchtMuenzen);
----

.*Lösung*
[%collapsible]
====
[source,java]
----
boolean nurNoten = (betrag % 10 == 0);
boolean brauchtMuenzen = !nurNoten;
System.out.println(brauchtMuenzen);
----

oder ganz kurz:

[source,java]
----
System.out.println(betrag % 10 != 0);
----
====
