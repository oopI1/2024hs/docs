= Programmieraufgaben
:sectnums:
:stem: latexmath
:icons: font

== Zinseszins interaktiv

Übernehmen Sie das Zinseszins-Programm aus den Vorlesungsfolien in Ihre Entwicklungsumgebung und erweitern Sie es so, dass es mittels `Scanner` den Anfangskontostand, den Zinssatz und eine Anzahl Jahre vom Benutzer einliest. Eine Interaktion mit dem Programm soll so aussehen:

----
Anfangskontostand (CHF)? 2500
Zinssatz (%)?            0.5
Anzahl Jahre?            50
Kontostand nach 50 Jahren: 3207.77
----

*Hinweis:* Ein paar eingegebenen Werte müssen noch umgewandelt
werden (Franken → Rappen, % → Dezimalzahl).


== Kunst mit der Turtle

Letzte Woche haben Sie mit der Turtle Vielecke gezeichnet. Ein solches Programm sah immer etwa so aus:

[source,java]
----
for (int i = 1; i <= seiten; i++) {
    forward(laenge);
    right(winkel);
}
----

Natürlich muss der Winkel die Zahl 360 teilen, damit ein Vieleck entsteht, z. B. 120°, 90° oder 60°. Nimmt man allerdings andere Winkel, entstehen interessante Figuren. Mit `winkel = 112` und `seiten = 45` entsteht z. B. folgende Zeichnung:

[.text-center]
image::res/turtle-art1.png[width=250]

Macht man die Seitenlänge bei jeder Wiederholung ein wenig grösser und ändert man während dem Zeichnen noch die Stiftfarbe, entstehen beeindruckende Zeichnungen. Zum Beispiel:

[.text-center]
image::res/turtle-art2.gif[width=300]

Ändern Sie das Programm `Kunst` in der Vorlage so ab, dass ein ähnliches Bild (oder was noch Beeindruckenderes) entsteht.

*Tipp:* Der Befehl zum Ändern der Stiftfarbe akzeptiert HSL-Strings (eigentlich https://de.wikipedia.org/wiki/HSV-Farbraum[HSV]), welche direkt Farbton, Sättigung und Helligkeit ausdrücken, z. B. so:

[source,java]
----
setPenColor("hsl(180,100%,100%)");
----

Die erste Zahl gibt einen Farbton zwischen 0 und 360 an, die weiteren beiden die Sättigung und Helligkeit zwischen 0 % und 100 %.


== «Kunst» mit ASCII-Zeichen

IMPORTANT: Das ist eine obligatorische Aufgabe und _muss eigenhändig und alleine gelöst werden._

Auch ohne Turtle kann man Bilder zeichnen, mittels https://de.wikipedia.org/wiki/ASCII-Art#Beispiele[ASCII-Art]. In dieser Aufgabe schreiben Sie ein Programm, das eine einfache ASCII-Zeichnung einer Tür auf der Konsole ausgibt, welche aber unterschiedlich gross sein kann:

[%autowidth.center]
|===
| Grösse | Zeichnung

| 0
a|----
_
----

| 1
a|
----
__
\|\|
----

| 2
a|
----
___
\| \|
\| \|
----

| 3
a|
----
____
\|  \|
\|  \|
\|  \|
----

| 4
a|
----
_____
\|   \|
\|   \|
\|   \|
\|   \|
----

|===

Und so weiter. Ihr Programm soll mittels `Scanner` zuerst den User nach einer Grösse fragen und anschliessend die Tür in dieser Grösse ausgeben. Zum Beispiel:

[subs="quotes"]
----
Grösse? *3*
\____
|  |
|  |
|  |
----

Vervollständigen Sie dazu das Programm `AsciiDoor` in Ihrem Repository. Es soll für alle möglichen Grössen (≥ 0) funktionieren, nicht nur für die oben dargestellten.

.*Tipps*
[%collapsible]
====
* Die Tür sollte Zeile für Zeile generiert werden. Dazu können Sie entweder zuerst eine Zeile als `String` zusammenbauen und dann mittels `System.out.println(...)` ausgeben. Oder Sie verwenden den Befehl `System.out.print(...)` (ohne `ln`), welcher etwas auf der Konsole ausgibt, _ohne danach eine neue Zeile zu starten_. So können Sie eine Zeile auch direkt in der Konsole «zusammenbauen».
* Beachten Sie die Regelmässigkeiten in den Beispielen oben: Für eine bestimmte Grösse _n_ braucht es auf beiden Seiten der Tür auch _n_× ein `|`-Zeichen. Hingegen braucht es oben (_n_ + 1)× ein `_`-Zeichen.
* Wenn Sie Schwierigkeiten mit dieser Aufgabe haben, versuchen Sie, das Programm Schritt für Schritt zu entwickeln, wie in der Vorlesung gezeigt: Zuerst schreiben Sie eine Version, z. B. für die Grösse 4, welche noch keine Schleifen verwendet. Dann schreiben Sie das Programm so um, dass Sie Wiederholungen im Code durch Schleifen ersetzen können.
====



== Binärdarstellung

Schreiben Sie ein Programm `Binaer`, das eine Zahl aus dem Dezimalsystem in das https://de.wikipedia.org/wiki/Dualsystem[Binär- oder Dualsystem] umwandelt. Die Ausgabe sollte z. B. so aussehen:

----
17 in Binaerdarstellung: 00010001
----

Am einfachsten kann man das lösen, indem man die Nullen und Einsen einzeln mit `System.out.print(...)` (ohne `ln`) ausgibt.

[loweralpha]
. Schreiben Sie zuerst eine Version des Programms, das keine Schleife verwendet, und verwenden Sie folgende Vorlage:
+
[source,java]
----
public static void main(String[] args) {
    int zahl = 17;
    System.out.print(zahl + " in Binärdarstellung: ");
    // TODO
}
----
+
Das Programm soll für eine beliebige ganze Zahl zwischen 0 und 255 (inklusive) die Binärdarstellung berechnen. Für diese Zahlen braucht es nicht mehr als 8 Stellen. Ändern Sie die Initialisierung von `zahl`, um verschiedene Zahlen zu testen.

. Schreiben Sie das Programm so um, dass es eine Schleife verwendet, um die einzelnen Stellen der Binärdarstellung zu berechnen. Am Ende sollte kein duplizierter Code mehr vorhanden sein.

. Schreiben Sie das Programm erneut um, sodass es die `zahl` mittels `Scanner` vom Benutzer einliest.

.*Tipps*
[%collapsible]
====
* Analysieren Sie folgende Beispiele, um zu verstehen, wie die Umwandlung funktionieren könnte:
+
----
  0 in Binaerdarstellung: 00000000
  1 in Binaerdarstellung: 00000001
  2 in Binaerdarstellung: 00000010
  3 in Binaerdarstellung: 00000011
  4 in Binaerdarstellung: 00000100
  5 in Binaerdarstellung: 00000101
  8 in Binaerdarstellung: 00001000
 15 in Binaerdarstellung: 00001111
 16 in Binaerdarstellung: 00010000
 89 in Binaerdarstellung: 01011001
255 in Binaerdarstellung: 11111111
----

* Denken Sie an den `%`-Operator und die Ganzzahldivision mit `/`, welche die Stellen hinter dem Komma abschneidet.
====


== Verschlüsselung

*Knacknuss:* In der Vorlesung haben Sie ein sehr einfaches Verschlüsselungsverfahren kennengelernt, die https://de.wikipedia.org/wiki/Caesar-Verschl%C3%BCsselung[Caesar-Verschlüsselung]. Diese ist in der Praxis hoffnungslos unsicher, da es nur wenige Schlüssel gibt. In dieser Aufgabe implementieren Sie ein (etwas) sichereres Verfahren, die https://de.wikipedia.org/wiki/Monoalphabetische_Substitution[monoalphabetische Substitution]. Hier wird jeder einzelne Buchstabe der zu verschlüsselnden Nachricht mithilfe eines Schlüsselalphabets durch irgendeinen anderen Buchstaben ersetzt.

Wenn zum Beispiel der Schlüssel `MGCWQJXETKYUZPABDFHILNORSV` verwendet wird, dann werden die Buchstaben nach folgender Tabelle, von oben nach unten, ersetzt:

|===
|Buchstabe:     | a | b | c | d | e | f | g | h | i | j | k | l | m | n | o | p | q | r | s | t | u | v | w | x | y | z
|Ersetzt durch: | M | G | C | W | Q | J | X | E | T | K | Y | U | Z | P | A | B | D | F | H | I | L | N | O | R | S | V
|===


Die Nachricht `executeordersixtysix` würde dadurch als `QRQCLIQAFWQFHTRISHTR` verschlüsselt werden. Um diese Nachricht wieder zu entschlüsseln, muss man den Schlüssel kennen; dann kann man die Tabelle umgekehrt, von unten nach oben, anwenden. (Die Gross- und Kleinschreibung dient nur Illustrationszwecken; man könnte auch eine Nachricht mit Gross- und Kleinbuchstaben verschlüsseln.)

Vervollständigen Sie das Programm `Monoalphabetisch` in Ihrem Repository, welches das Verfahren durchführen soll. Wenn Ihre Lösung korrekt ist, sollte die
Interaktion z. B. so aussehen:

    Schlüssel?     MGCWQJXETKYUZPABDFHILNORSV
    Nachricht?     executeordersixtysix
    Verschlüsselt: QRQCLIQAFWQFHTRISHTR

Sie dürfen davon ausgehen, dass die zu verschlüsselnde Nachricht immer nur Kleinbuchstaben von `a` bis `z` enthält (keine `ä`, `ö`, `ü`, Leer-, Satz- oder andere Zeichen) und der String für den Schlüssel immer genau 26 verschiedene Zeichen enthält.

.*Tipp*
[%collapsible]
====
Um einen einzelnen Buchstaben zu ersetzen, müssen Sie zuerst dessen Position im (normalen) Alphabet herausfinden (`a` hat Position 0, `z` hat Position 25). Verwenden Sie dafür die Umwandlung von `char` nach `int`. Diese Position können Sie verwenden, um den entsprechenden Buchstaben im Schlüsselalphabet zu finden.
====
