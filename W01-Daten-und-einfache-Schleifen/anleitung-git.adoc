= Anleitung: Code beziehen und abgeben via Git
:sectnums:
:icons: font

NOTE: Sie können dieser Anleitung nur folgen, wenn Sie einen Account auf GitLab haben (siehe https://css.pages.fhnw.ch/einfuehrungswoche/2024hs/module/oopI1-1iCa/[Anleitung der Einführungswoche]). Falls Sie sich nicht während der Einführungswoche auf GitLab registriert haben, schreiben Sie nach der Registrierung eine E-Mail an michael.faes@fhnw.ch.


== Code-Vorlagen beziehen

Die Vorlagen für die Programmieraufgaben befinden sich in Ihrem privaten Git-Repository. Dieses werden Sie nun klonen.

[loweralpha]
. Schliessen Sie das aktuell geöffnete Projekt in IntelliJ und wählen Sie _Get from VCS_ oben rechts. Geben Sie bei _URL_ folgende URL ein *und ersetzen `vorname.nachname` mit Ihrem eigenen FHNW-Kürzel*:
+
----
https://gitlab.fhnw.ch/oopI1/2024hs/code/oopI1-code_vorname.nachname.git
----
+
Drücken Sie anschliessend _Clone_. Da dies ein privates Repository ist, müssen Sie dieses Mal Ihre Benutzerdaten für GitLab eingeben:
+
image:res/credentials.png[width=500]
+
Sie sollten nun ein Projekt mit einem Ordner «aufgaben-01» haben:
+
image:res/cloned-aufgaben.png[width=600]

. Einige Standardeinstellungen von IntelliJ sind für Einsteiger nicht geeignet. Wir haben deshalb eine Sammlung von hilfreichen Einstellungen zusammengestellt, welche Sie importieren sollten. Klicken Sie dazu oben links auf das Menu (vier Balken) und wählen Sie _File_ → _Manage IDE Settings_ → _Import Settings_.
+
image:res/import-settings.png[width=600]
+
Navigieren Sie zur Datei «settings.zip», welche sich in Ihrem soeben geklonten Repository befindet, und drücken Sie _OK_.
+
image:res/select-settings-file.png[width=600]
+
Stellen Sie sicher, dass im folgenden Dialog alle Checkboxen ausgewählt sind, und drücken Sie erneut _OK_ und schliesslich _Import and Restart_.

. Als Letztes müssen Sie die Aufgabenvorlage für Woche 1, welche bisher ein normaler Ordner ist, noch in ein Java-Projekt umwandeln. Rechtsklicken Sie dazu auf die Datei «pom.xml» im Ordner «aufgaben-01» und wählen Sie _Add as Maven Project_:
+
image:res/add-as-maven-project.png[width=600]
+
(_Maven_ ist eine Software, um Java-Projekte zu verwalten und zu kompilieren. Wir brauchen Maven nur, um die Übungsvorlagen in IntelliJ zu importieren.)

. Nachdem IntelliJ einige weitere Dinge geladen hat (beachten Sie den Ladebalken unten), sollten Sie die Java-Programme in dem Projekt ausführen können. Öffnen Sie z. B. das Programm `Dreieck` (im Ordner «src») und führen Sie es aus:
+
image:res/turtle.png[width=600]


== Änderungen committen und pushen

Jetzt sollen Sie eine Änderung an einem Programm vornehmen, committen und zurück auf den GitLab-Server pushen. Auf diese Art geben Sie später Ihre Lösungen zu den Programmieraufgaben ab.

[loweralpha]
. Ändern Sie das Programm `Dreieck` so ab, dass ein doppelt so grosses Dreieck gezeichnet wird. Danach drücken Sie oben auf _main_ und dann auf _Commit_:
+
image:res/commit.png[width=600]
+
Es öffnet sich links der Commit-Dialog, indem Sie die Änderungen prüfen und eine Commit-Nachricht eingeben können. Beschreiben Sie die Änderung kurz und drücken Sie dann auf _Commit_. Es öffnet sich ein Dialog, wo Sie einen Git-Benutzernamen angeben können. Dieser wird im Commit hinterlegt:
+
image:res/git-user-name.png[width=600]

. Nach dem Committen ist Ihre Änderung in der **lokalen** History gespeichert. Sie können sie ansehen, indem Sie unten links den _Git_-Tab einblenden:
+
image:res/git-button.png[width=300]
+
image:res/history.png[width=600]

. Um die Änderung auf den Server zu laden, müssen Sie sie zusätzlich noch _pushen_. Drücken Sie dazu nochmals oben auf _main_ und dann auf _Push_:
+
image:res/push.png[width=600]
+
image:res/push-2.png[width=600]
+
Anschliessend sollten Sie Ihre Änderung auf der GitLab-Website in Ihrem Repository sehen können:
+
image:res/push-3.png[width=600]


