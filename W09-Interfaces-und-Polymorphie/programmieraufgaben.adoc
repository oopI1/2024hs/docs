= Programmieraufgaben
:stem: latexmath
:icons: font
:hide-uri-scheme:
:sectnums:


== Labyrinth

IMPORTANT: Das ist eine obligatorische Aufgabe und _muss eigenhändig und alleine gelöst werden._

In dieser Aufgabe sollen Sie verschiedene Algorithmen entwerfen, um in Labyrinthen ein Ziel zu finden. Sie finden in der Vorlage für diese Woche bereits ein vollständiges Programm `LabyrinthApp`, welches eine Figur in einem Labyrinth anzeigt. Stellen Sie es sich als eine Art Spiel mit mehrere Levels vor – ausser, dass Sie nur ein einziges Mal ins Spiel eingreifen können: ganz am Anfang, wenn Sie den Algorithmus angeben.

Zur Einstimmung in diese Aufgabe können Sie hier einmal die Algorithmen ausprobieren: https://blockly.games/maze
[.text-center]
image:res/labyrinth.png[width=400]

Dasselbe Grundprogramm mit verschiedenen Algorithmen durchzuführen ist ein Paradebeispiel für die Verwendung von Interfaces. Entsprechend geben Sie Ihre Navigations-Algorithmen an, indem Sie mehrere Klassen schreiben, die alle ein vorgegebenes Interface implementieren.

[loweralpha]
. Starten Sie `LabyrinthApp`. Es wird das erste Level angezeigt, aber die Figur bewegt sich noch nicht, da in `LabyrinthApp` noch kein Navigations-Algorithmus definiert wurde. Erstellen Sie als Erstes eine Klasse `StupidAlgorithm`, welche das `NaviAlgorithm`-Interface mit der Methode `navigate` implementiert. Die Methode bekommt als Argument ein `Figure`-Objekt, welches die Figur im Labyrinth repräsentiert. Sie können die Figur mit `turnLeft()`, `turnRight()`, und `moveForward()` steuern. Ausserdem können Sie Abfragen machen: `pathAhead()`, `pathToTheLeft()`, `pathToTheRight()` und `isGoalReached()`. Ihr erster Versuch, `StupidAlgorithm`, soll mit einer `while`-Schleife einfach so lange _geradeaus_ steuern, bis das Ziel erreicht ist. Achten Sie darauf, die Klasse `StupidAlgorithm` (und alle weiteren für diese Aufgabe) im Package `labyrinth` zu erstellen, sonst funktionieren die Tests auf dem Grading Server nicht.
+
Erstellen Sie dann in `LabyrinthApp` an der Stelle `TODO` ein `StupidAlgorithm`-Objekt. Damit sollten Sie das erste Level schaffen (das zweite wird allerdings crashen).

. Schreiben Sie als Nächstes einen Algorithmus namens `TryStraightFirst`, welcher in jedem Schleifendurchlauf überprüft, ob es geradeaus weitergeht, und in dem Fall einen Schritt nach vorne macht. Falls es keinen Pfad geradeaus gibt, wird links oder rechts versucht. Sollte das alles nicht gehen, soll die Figur rechtsum kehrt machen.
+
Mit diesem Algorithmus sollten Sie die ersten vier, vielleicht auch das fünfte Level schaffen. Denken Sie daran in der `main`-Methode den `StupidAlgorithm` mit dem `TryStraightFirst` zu ersetzen.

. Für die letzten beiden Levels müssen Sie sich noch etwas besseres überlegen. Finden Sie einen allgemeinen Algorithmus, der für alle diese Labyrinthe funktioniert und implementieren Sie ihn in einer Klasse `SuperAlgorithm`.

Zur Abgabe gehören alle drei Klassen `StupidAlgorithm`, `TryStraightFirst` und
`SuperAlgorithm`. Falls Sie beim Super-Algorithmus nicht weiterkommen, hilft
https://de.wikipedia.org/wiki/L%C3%B6sungsalgorithmen_f%C3%BCr_Irrg%C3%A4rten[Wikipedia].

Für die drei Klassen sind auch Unit-Tests vorgegeben, welche testen, ob der Algorithmus die entsprechenden Levels besteht. Die Tests sind wieder auskommentiert, da die Klassen zu Beginn ja noch nicht vorhanden sind und der Code deshalb nicht kompilieren würde.


== Datenvisualisierung

IMPORTANT: Das ist eine obligatorische Aufgabe und _muss eigenhändig und alleine gelöst werden._

In dieser Aufgaben werden Sie drei verschiedene Datensätze visualisieren und dabei nochmals mit einem Interface arbeiten.

[.text-center]
image:res/movies.png[width=300] image:res/movies-hover.png[width=300]

[.text-center]
image:res/countries.png[width=300] image:res/countries-hover.png[width=300]

[.text-center]
image:res/processors.png[width=300] image:res/processors-hover.png[width=300]

Im Package `visualizer` finden Sie die Klasse `Visualizer`, welche noch auskommentiert ist. Diese implementiert die ganze Logik, um Datensätze wie oben darzustellen. Zudem sind bereits drei Klassen `Movie`, `Country` und `Processor` vorhanden, welche die darzustellenden Daten speichern. Ihre Aufgabe liegt darin, diese drei Klassen mit dem `Visualizer` kompatibel zu machen. Dazu erstellen Sie ein Interface `DataPoint`, welche als Schnittstelle zum `Visualizer` funktionieren soll: Klassen, welche mit dem `Visualizer` dargestellt werden sollen, müssen dieses Interface implementieren. Dadurch ist es möglich, mehrere Datensätze mit völlig unterschiedlichen Attributen mit einer einzigen `Visualizer`-Implementation zu visualisieren!

[loweralpha]
. Erstellen Sie ein Interface namens `DataPoint` (im Package `visualizer`!) und entfernen Sie die Kommentarzeichen in der Klasse `Visualizer`. Dadurch sehen Sie, welche Methoden das `DataPoint`-Interface zur Verfügung stellen muss, dass der `Visualizer` funktioniert:
+
[%autowidth]
|===
| Methode | Rückgabetyp | Beschreibung

| `getFirstValue` | `double` | Gibt den Wert des Datenpunktes für die _x_-Achse zurück.
| `getSecondValue` | `double` | Gibt den Wert für die _y_-Achse zurück. Für jedes `DataPoint`-Objekt wird entsprechend den Werten von `getFirstValue` und `getSecondValue` ein Punkt im Diagramm eingezeichnet.
| `getGroup` | `String` | Gibt die «Gruppe» des Datenpunkts zurück. Der Datenpunkt wird entsprechend seiner Gruppe eingefärbt. Der `Visualizer` sammelt zudem alle Gruppen im Datensatz und erstellt automatisch eine Legende.
| `getLabel` | `String` | Gibt die Beschriftung des Datenpunkts zurück, für Länder z. B. einfach der Name des Landes.
| `getDescription` | `String` | Gibt die Detailbeschreibung des Datenpunkts zurück. Wenn man mit der Maus über einen Datenpunkt fährt, wird dessen Beschriftung und darunter die Detailbeschreibung angezeigt.
|===
+
Die `Visualizer`-Klasse sollte jetzt kompilieren.

. Machen Sie den Film-Datensatz kompatibel mit dem `Visualizer`, indem Sie die `Movie`-Klasse so erweitern, dass sie das `DataPoint`-Interface implementiert. Als ersten Wert soll das Filmbudget, als zweiten Wert das User-Rating zurückgegeben werden. Als Gruppe soll das Jahrzehnt des Films zurückgegeben werden, als Beschriftung der Titel und das Jahr und als Detailbeschreibung das Budget und das Rating. Der Screenshot oben zeigt, wie diese Dinge im Detail formatiert werden sollen.
+
Ändern Sie dann die `main`-Methode in `VisualizerApp`, sodass als Datensatz die Filme übergeben werden, welche durch `loadMovies` geladen werden. Jetzt sollten Sie das Programm starten und die Visualisierung der Filme sehen können. Entsprecht die Darstellung Ihren Erwartungen?

. Mit dem Länder-Datensatz wollen wir untersuchen, welchen Einfluss die Lesefähigkeit (_literacy_) der Bevölkerung eines Landes auf dessen Bruttoinlandprodukt pro Kopf (_GDP per capita_) hat. Erweitern Sie dazu die `Country`-Klasse so, dass sie ebenfalls das `DataPoint`-Interface implementiert und diese beiden Informationen als ersten und zweiten Wert zurückgibt. Entnehmen Sie die Details zu Beschriftung, usw. wieder dem entsprechenden Screenshot oben. Ändern Sie nochmals die `main`-Methode um die Länder anstelle der Filme zu visualisieren.

. Als Letztes wollen wir untersuchen, wie sich die Rechengeschwindigkeit von Computerprozessoren zwischen 1971 und 2014 entwickelt hat. Dazu erweitern Sie die `Processor`-Klasse, analog zu `Movie` und `Country`. Bei diesem Datensatz gibt es allerdings ein paar Besonderheiten:
+
Als erster Wert (_x_-Achse) soll der Veröffentlichungsmonat verwendet werden. Kombinieren Sie dazu das Jahr und den Monat zu einem einzigen Wert: stem:[total = 12 \times jahr + monat].
+
Als zweiter Wert (_y_-Achse) soll die «effektive Rechengeschwindigkeit» verwendet werden. Als Vereinfachung multiplizieren Sie dafür einfach die Taktfrequenz (_clock rate_) mit der Anzahl Cores. Ein 2.5-GHz-Prozessor mit zwei Cores hätte damit eine effektive Geschwindigkeit von 5.0 GHz. Da die Geschwindigkeit von Prozessoren über die Jahre etwa _exponentiell_ ansteigt (siehe https://de.wikipedia.org/wiki/Mooresches_Gesetz[Moore's law]), muss man für eine gute Darstellung eine logarithmische _y_-Achse verwenden. Verwenden Sie deshalb `Math.log(...)` um den Logarithmus der effektiven Geschwindigkeit als _y_-Wert zurückzugeben.
+
Die Detailbeschreibung enthält auf der ersten Zeile den Monat und das Jahr und auf der zweiten die Anzahl Cores und die Taktfrequenz. Letztere soll in eine geeignete Einheit (kHz, MHz oder GHz) umgerechnet werden; kHz- und MHz-Werte werden dabei als ganze Zahlen (z. B. 800 MHz), GHz-Werte als Kommazahlen angezeigt (z. B. 2.5 GHz).

Für die Klassen `Movie`, `Country` und `Processor` sind auch (auskommentierte) Unit-Tests vorhanden, welche testen, dass die Klassen das `DataPoint`-Interface wie oben beschrieben implementieren.
