= Repetition Woche 9: Interfaces
:sectnums:
:stem: latexmath

Die Aufgaben zur heutigen Repetition finden Sie einerseits in Form eines Quizzes und andererseits als Implementationsaufgabe. Alle Aufgaben arbeiten mit den folgenden Interfaces:

[source, java]
----
public interface InterfaceA {
    void methodeA1();
    int methodeA2();
    String[] methodeA3(String text, int value);
}

public interface InterfaceB {
    void methodeB1();
    int methodeB2();
    double methodeB3(double[] values);
}
----

== Quiz

https://forms.office.com/Pages/ResponsePage.aspx?id=yF8anR4yAUGuY1MHMHEawoOl-vNson1Jjooz0m4wSyFUNDZIUkkxUEdPV0FDQjlRUzdCOTkyUk9MTS4u

== Programmieraufgaben

Erstellen Sie eine Klasse `Main`. In dieser sollen dann die folgenden Methoden geschrieben werden:

=== Methode für `InterfaceA`
Implementieren Sie eine statische Methode `sumMethodeA2` in der Klasse `Main`, welche ein Array von InterfaceA-Referenzen bekommt und die Summe der Resultate von `methodeA2()` berechnet und zurückgibt.

=== Klasse `B` für `InterfaceB`
Implementieren Sie eine Klasse `B`, welche das Interface `InterfaceB` implementiert.

- `methodeB1`: Gibt den Buchstaben `B` auf der Konsole aus.
- `methodeB2`: Gibt `0` zurück.
- `methodeB3`: Gibt zurück, wie gross das Array `values` ist.

== Zusatz: Ente soll Fliegen und Tauchen lernen
Sie haben im Package `repetition` eine Klasse `Ente` sowie die Interfaces `Fliegen` und `Tauchen`.

Implementieren Sie eine Klasse `Ente`, welche die Methoden `methodeFliegen` und `methodeTauchen` der Interfaces `Fliegen` und `Tauchen` implementiert. Die Methoden sollen "Fliegen" und "Tauchen" auf der Konsole ausgeben.
