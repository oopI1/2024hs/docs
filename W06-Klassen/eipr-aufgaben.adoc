= EIPR-Aufgaben
:sectnums:
:stem: latexmath
:icons: font


== Klassen-Code lesen

In der Vorlage von dieser Woche finden Sie eine Version der `Modul`-Klasse aus der Vorlesung. Lesen Sie die Klasse genau durch und beantworten Sie folgende Fragen:

[loweralpha]
. Richtig oder falsch? Von den Variablen `name`, `credits`, `erfNote` und `mspNote` kann es zur Laufzeit eines Programms, das die `Modul`-Klasse verwendet, mehrere Kopien geben.
. Richtig oder falsch? Im Konstruktor der Klasse (`public Modul(...)`) könnte man das `this` weglassen und die Klasse würde immer noch _kompilieren_.
. Richtig oder falsch? In der Methode `bestanden()` könnte man statt `endNote()` auch `this.endNote()` schreiben, ohne etwas an der Bedeutung des Codes zu ändern.
. Angenommen, in einem Programm wird ein Objekt der Klasse `Modul` erstellt:
+
[source,java]
----
Modul m = new Modul("oopI1", 3);
----
+
Wie ruft man die Methode `endNote()` auf einem Objekt `m` der Klasse `Modul` korrekt auf?
+
** `double note = Modul.endNote();`
** `double note = Modul.endNote(m);`
** `double note = m.endNote;`
** `double note = m.endNote();`

. Welcher Wert würde in der Variablen `note` stehen, wenn die Methode `endNote()` direkt nach der Erstellung des Objekts in der Variable `m` aufgerufen wird?

. Welchen Effekt hätte es, wenn man bei der Deklaration der Methode `endNote()` das Schlüsselwort `static` hinzufügen würde?

.*Lösung*
[%collapsible]
====
[loweralpha]
. *Richtig.* Wenn mehrere Instanzen der Klasse `Modul` erstellt werden, gibt es für jede Instanz eigene Kopien der Variablen `name`, `credits`, `erfNote` und `mspNote`.
. *Richtig*, die Klasse würde noch kompilieren, aber nicht mehr richtig _funktionieren_, denn der Konstruktor würde die Parameter `name` und `credits` sich selbst zuweisen, anstatt die Attribute zu initialisieren.
. *Richtig.* `endNote()` ist ein Aufruf einer Instanz-Methode auf dasselbe Objekt, auf das die `bestanden()`-Methode aufgerufen wird. Man kann das `this` bei solchen Aufrufen schreiben oder weglassen.
. `double note = m.endNote();` Instanzmethoden werden immer _auf einem Objekt_ aufgerufen, mittels Dot-Notation.
. Der Wert in der Variablen `note` wäre `0.0`, denn der Konstruktor von `Modul` weist den Attributen `erfNote` und `mspNote` keinen Wert zu, wodurch sie Null-initialisiert werden. `(0.0 + 0.0) / 2` gibt ebenfalls `0.0`.
. Das würde zu Compile-Fehlern in der Methode `endNote()` führen, da `static`-Methoden nicht auf Attribute zugreifen können. Es wäre ja auch nicht klar, auf die Attribute welcher Instanz sich die Methode beziehen soll, da `static`-Methoden nicht auf Instanzen aufgerufen werden.
====


== Objekte von eigenen Klassen verwenden

Ebenfalls in der Vorlage befindet sich eine Version des Programms `Noten`, dessen Verwendung in der Vorlesung gezeigt wurde. Im Moment verhält sich das Programm wie folgt (User-Eingaben sind fett markiert):

[subs="quotes"]
----
Erf-Note für oopI1? *4.7*
MSP-Note für oopI1? *3.8*
Erf-Note für sweGL? *2.8*
MSP-Note für sweGL? *4.2*

oopI1: 4.25
sweGL: 3.5

Bestandene Module:    1
----

Erweitern Sie das Programm folgendermassen:

[loweralpha]
. Fügen Sie zwei zusätzliche Module «webeC» und «mgli» hinzu. Beide geben 3 Credits.

. Erweitern Sie die Programmlogik, sodass die Anzahl der gesammelten Credits und der Notendurchschnitt der bestandenen Module berechnet und ausgegeben werden:
+
[subs="quotes"]
----
Erf-Note für oopI1? *4.7*
MSP-Note für oopI1? *3.8*
Erf-Note für sweGL? *2.8*
MSP-Note für sweGL? *4.2*
Erf-Note für webeC? *5.2*
MSP-Note für webeC? *4.4*
Erf-Note für mgli? *5.6*
MSP-Note für mgli? *4.4*

oopI1: 4.3
sweGL: 3.5
webeC: 4.8
mgli: 5.0

Bestandene Module:    3
Gesammelte Credits:   9
Schnitt (bestandene): 4.7
----


== Instanz-Methode umschreiben

Ändern Sie die Implementation der Methode `endNote` in der Klasse `Modul`, sodass sie folgende Anforderungen erfüllt:

. Die Endnote soll auf _eine_ Nachkommastelle gerundet werden. Beispiel: Für die Noten 4.5 und 4.2 ist der Durchschnitt 4.35, der auf 4.4 gerundet werden soll.
+
*Tipp:* `Math.round(x)` rundet eine Zahl `x` auf eine _ganze Zahl_. Wie können Sie diese Methode verwenden, um auf _eine Nachkommastelle_ zu runden?

. Falls die Erfahrungsnote oder die MSP-Note noch 0 ist, wird als Endnote ebenfalls 0 zurückgegeben werden.
