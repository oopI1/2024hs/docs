= Woche 10: Vererbung I

link:Vererbung%20I.pdf[Vorlesungsfolien]

xref:skript.adoc[Skript]


== Aufgaben

xref:uebungsaufgaben.adoc[Übungsaufgaben]

xref:eipr-aufgaben.adoc[EIPR-Aufgaben]

xref:programmieraufgaben.adoc[Programmieraufgaben]

xref:RepetitionsblattABCD.pdf[Repetitionsaufgaben]


== Obligatorische Aufgaben

Die obligatorischen Aufgaben für diese Woche sind xref:programmieraufgaben.adoc#_to_do_app_erweitern[To-Do-App erweitern] und xref:programmieraufgaben.adoc#_real_estate[Real Estate]. Diese werden wieder durch den https://www.cs.technik.fhnw.ch/grading-server/[Grading-Server] bewertet.

Der Abgabetermin ist am *23.11.2024, um 23:59*.


== Standortbestimmung

Der Abgabetermin für das Quiz von dieser Woche ist ebenfalls am *23.11.2024, um 23:59*.

→ https://api.socrative.com/rc/iBJcjQ[Zum Quiz für Woche 10]


== Zusatzmaterial für die Projektwoche

Falls Sie Zeit haben, empfehlen wir, dass Sie sich in der Projektwoche schon etwas mit der Vorbereitung auf die zweite Midterm-Prüfung beschäftigen. Dazu haben wir einige Aufgaben vorbereitet, die Sie unter xref:../Vorbereitung-Midterm-2/index.adoc[Vorbereitung Midterm 2] finden.
