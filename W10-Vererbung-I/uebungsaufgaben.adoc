= Übungsaufgaben
:stem: latexmath
:icons: font
:hide-uri-scheme:
:sectnums:


== Spezialisierter Ticket-Typ (Vorlesung)

Wir möchten einen neuen Ticket-Typ zum Programm im Package `ticket` hinzufügen, z. B. ein `Sparticket`. Spartickets funktionieren gleich wie normale Tickets, ausser, dass sie zusätzlich einen `rabatt` speichern, welcher den `preis()` entsprechend reduziert.

Diskutieren Sie zu zweit:

[loweralpha]
. Welche Schritte wären nötig, um diese Erweiterung auf die gleiche Art zu implementieren wie letzte Woche beim Parking-System?
. Welche Probleme ergeben sich mit diesem Ansatz?
. Finden Sie eine Lösung für diese Probleme mit den Mitteln, die wir bisher kennen?

.*Lösungen*
[%collapsible]
====
[loweralpha]
. {empty}
+
1. Klasse Ticket umbenennen, z. B. zu `Standardticket`
2. Interface `Ticket` mit Methode `preis()` erstellen
3. `Standardticket` ändern: `implements Ticket`
4. Klasse `Sparticket implements Ticket` erstellen
5. Attribute, Konstruktor, etc. von `Standardticket` kopieren…

. Viel duplizierter Code in `Normalticket` & `Sparticket`

. Könnten Attribute in eigene Klasse `Ticketdaten` packen:
+
[source,java]
----
public class Standardticket implements Ticket {
    private Ticketdaten daten;

    public Ticket(Ticketdaten daten) {
        this.daten = daten;
    }
    ...
----
+
[source,java]
----
public class Sparticket implements Ticket {
    private Ticketdaten daten;
    private double rabatt;
    ...
----
====


== Spezialisierung modellieren (Vorlesung)

Finden Sie zu zweit weitere Beispiele für Spezialisierung aus der echten Welt oder in Software-Systemen und modellieren Sie sie wie das Beispiel in den Folien als Vererbungshierarchie.

.*Weiteres Beispiel*
[%collapsible]
====
[source,java]
----
public class Land {
    private String name;
    private int einwohner;
    ...
----

[source,java]
----
public class Monarchie extends Land {
    private String koenig;
    ...
}
----

[source,java]
----
public class Republik extends Land {
    private String[] regierung;
    ...
}
----
====


== Sparticket implementieren (Vorlesung)

[loweralpha]
. Implementieren Sie eine neue Klasse `Sparticket` in Ihrer Vorlage, indem Sie die Dinge auf den Folien bis und mit 16 kombinieren. Erweitern Sie dann das Programm `TicketShop` so, dass auch einige `Sparticket`-Objekte erstellt werden:
+
[source,java]
----
Ticket[] tickets = new Ticket[5];

tickets[0] = new Ticket("Tellplatz", 2, false);
tickets[1] = new Ticket("Schützenhaus", 5, false);
tickets[2] = new Ticket("Basel SBB", 3, true);
tickets[3] = new Sparticket("Aesch Dorf", 2, false, 20);
tickets[4] = new Sparticket("Frick", 8, true, 15);
----

. Wo stossen Sie auf Probleme/Unschönheiten?

.*Lösungen*
[%collapsible]
====
[loweralpha]
. {empty}
+
[source,java]
----
public class Sparticket extends Ticket {
    private double rabatt;

    public Sparticket(String abfahrtsort, int zonen,
                      boolean ersteKlasse, int rabatt) {
        super(abfahrtsort, zonen, ersteKlasse);
        this.rabatt = rabatt;
    }

    @Override
    public double preis() {
        double multiplikator = 1;
        if (isErsteKlasse()) {
            multiplikator = 1.7;
        }
        double origUngerundet = (GRUNDPREIS + getZonen() * PREIS_PRO_ZONE)
                * multiplikator;
        double orig = Math.round(origUngerundet * 20) / 20.0;

        double ungerundet = orig * (1 - rabatt / 100.0);
        return Math.round(ungerundet * 20) / 20.0;
    }
}
----

. Die Methode `preis` in `Sparticket` ist fast, aber eben nicht ganz identisch mit derjenigen in `Ticket`. Dadurch entsteht viel duplizierter Code. Das wird in späteren Übungen noch verbessert.
====


== `super`-Aufrufe (Vorlesung)

Übernehmen Sie die `preis`-Methode mit dem `super`-Aufruf aus den Folien in die `Sparticket`-Klasse. Überprüfen Sie die Ausgabe:

[source,java]
----
Einzelticket 2. Klasse ab Tellplatz, 2 Zonen: CHF 5.30
Einzelticket 2. Klasse ab Schützenhaus, 5 Zonen: CHF 9.80
Einzelticket 1. Klasse ab Basel SBB, 3 Zonen: CHF 11.55
Einzelticket 2. Klasse ab Aesch Dorf, 2 Zonen: CHF 4.25
Einzelticket 1. Klasse ab Frick, 8 Zonen: CHF 20.65
----

Überschreiben Sie nun die `toString`-Methode in `Sparticket`, sodass sie folgendes ausgibt. Verwenden Sie nochmals `super`.

[source,java]
----
Einzelticket 2. Klasse ab Tellplatz, 2 Zonen: CHF 5.30
Einzelticket 2. Klasse ab Schützenhaus, 5 Zonen: CHF 9.80
Einzelticket 1. Klasse ab Basel SBB, 3 Zonen: CHF 11.55
Sparticket 2. Klasse ab Aesch Dorf, 2 Zonen: CHF 4.25 (-20%)
Sparticket 1. Klasse ab Frick, 8 Zonen: CHF 20.65 (-15%)
----

*Tipp:* Mit der Methode `string.replace(x, y)` kann man in einem String alle Vorkommnisse von `x` durch `y` ersetzen.

.*Lösung*
[%collapsible]
====
[source,java]
----
public class Sparticket extends Ticket {

    private final int rabatt;

    public Sparticket(String abfahrtsort, int zonen, boolean ersteKlasse,
                      int rabatt) {
        super(abfahrtsort, zonen, ersteKlasse);
        this.rabatt = rabatt;
    }

    @Override
    public double preis() {
        double ungerundet = super.preis() * (1 - rabatt / 100.0);
        return Math.round(ungerundet * 20) / 20.0;
    }

    @Override
    public String toString() {
        String original = super.toString();
        String replaced = original.replace("Einzelticket", "Sparticket");
        return replaced + " (-" + rabatt + "%)";
    }
}
----

Die `toString`-Methode kann man auch in einer (eher langen) Zeile schreiben:

[source,java]
----
@Override
public String toString() {
    return super.toString().replace("Einzelticket", "Sparticket") + " (-" + rabatt + "%)";
}
----
====


== Vererbung und Polymorphie

Gegeben seien folgende Klassen:

[source,java]
----
public class Monster {
    public void makeSound() {
        System.out.println("Groar!");
    }
}
----

[source,java]
----
public class Pokemon extends Monster {
    public String pokedexNum() {
        return "MISSINGNO.";
    }
}
----

[source,java]
----
public class Bulbasaur extends Pokemon {
    public void makeSound() {
        System.out.println("Bulbasaur!");
    }
    public String pokedexNum() {
        return "001";
    }
}
----

[source,java]
----
public class Pikachu extends Pokemon {
    public void makeSound() {
        System.out.println("Pika pika!");
    }
    public String pokedexNum() {
        return "025";
    }
}
----

Bestimmen Sie für jedes folgende Code-Stück, ob es fehlerfrei ist, und, wenn ja, welche Ausgabe es produziert.

[source,java]
----
Pikachu p = new Pikachu();
System.out.print(p.pokedexNum() + ": ");
p.makeSound();
Monster m = p;
System.out.print("Monster: ");
m.makeSound();
----

[source,java]
----
Pokemon p = new Bulbasaur();
System.out.print(p.pokedexNum() + ": ");
p.makeSound();
p = new Pikachu();
System.out.print(p.pokedexNum() + ": ");
p.makeSound();
----

[source,java]
----
Pokemon p = new Pokemon();
System.out.print(p.pokedexNum() + ": ");
p.makeSound();
Monster m = new Monster();
System.out.print("Monster: ");
m.makeSound();
----

[source,java]
----
Monster m = new Pokemon();
System.out.print(m.pokedexNum() + ": ");
m.makeSound();
m = new Pikachu();
System.out.print(m.pokedexNum() + ": ");
m.makeSound();
----

.*Lösungen*
[%collapsible]
====
[loweralpha]
. {empty}
+
----
025: Pika pika!
Monster: Pika pika!
----

. {empty}
+
----
001: Bulbasaur!
025: Pika pika!
----

. {empty}
+
----
MISSINGNO.: Groar!
Monster: Groar!
----

. Fehler: Variable `m` hat den Typ `Monster`, deshalb kann man auf `m` die Methode `pokedexNum` nicht aufrufen (auch wenn ein Objekt der Klasse `Pokemon` in der Variable ist – _der Compiler schaut nur auf den Variablentyp_, siehe letzte Woche).
====


== Up- und Down-Casts

Gegeben seien nochmals die gleichen Interfaces und Klassen wie in einer Aufgabe von letzter Woche:

[source,java]
----
public interface Magical {
    String name();
    int power();
}
----

[source,java]
----
public interface Wizard extends Magical {
    void printSlogan();
}
----

[source,java]
----
public class Gandalf implements Wizard {
    public String name() {
        return "Gandalf the Grey";
    }
    public int power() {
        return 9001;
    }
    public void printSlogan() {
        System.out.println("You shall not pass!");
    }
}
----

[source,java]
----
public class Balrog implements Magical {
    public String name() {
        return "Durin's Bane";
    }
    public int power() {
        return 9000;
    }
    public void makeSound() {
        System.out.println("GROAR!");
    }
}
----

Entscheiden Sie für jedes der folgenden Codestücke, in welche Kategorie es passt:

* fehlerfrei
* fehlerfrei, aber enthält einen unnötigen _Up-Cast_
* erzeugt einen Kompilierfehler
* wirft bei der Ausführung eine `ClassCastException`

[loweralpha]
. {empty}
+
[source,java]
----
Gandalf g = new Gandalf();
Wizard w = g;
----

. {empty}
+
[source,java]
----
Balrog b = new Balrog();
Wizard w = b;
----

. {empty}
+
[source,java]
----
Wizard w = new Gandalf();
Magical m = (Magical) w;
----

. {empty}
+
[source,java]
----
Gandalf g = new Gandalf();
Magical m = g;
----

. {empty}
+
[source,java]
----
Wizard w = new Gandalf();
Gandalf g = w;
----

. {empty}
+
[source,java]
----
Wizard w = new Gandalf();
Gandalf g = (Gandalf) w;
----

. {empty}
+
[source,java]
----
Balrog b = new Balrog();
Magical m = b;
----

. {empty}
+
[source,java]
----
Magical m = new Balrog();
Wizard w = (Wizard) m;
----

.*Lösungen*
[%collapsible]
====
[loweralpha]
. fehlerfrei
. Kompilierfehler: `b` vom Typ `Balrog` kann man nicht einer Variable vom Typ `Wizard` zuweisen, denn `Balrog` ist kein Subtyp von `Wizard`.
. fehlerfrei, aber der Cast von `Wizard` zu `Magical` ist ein unnötiger Up-Cast, da `Wizard` ein Subtyp von `Magical` ist.
. fehlerfrei
. Kompilierfehler: `w` vom Typ `Wizard` kann man nicht einer Variable vom Typ `Gandalf` zuweisen, denn `Wizard` ist kein Subtyp von `Gandalf`. Nochmals: Es spielt keine Rolle, dass `Gandalf` zur Laufzeit ein Objekt vom Typ `Wizard` ist; der Compiler schaut nur auf den Variablentyp.
. fehlerfrei: Der Cast von `Wizard` zu `Gandalf` ist ein Down-Cast, der zur Laufzeit funktioniert, da `w` ein `Gandalf`-Objekt enthält.
. fehlerfrei
. `ClassCastException`: `m` enthält ein `Balrog`-Objekt, das man nicht zum Typ `Wizard` casten kann. Mit dem Cast sagen wir dem Compiler im Prinzip: «Ich weiss, dass ich `m` nicht direkt als Typ `Wizard` verwenden kann, aber ich weiss, dass `m` zur Laufzeit ein Objekt enthält, das zu `Wizard` kompatibel ist.» In diesem Fall stimmt das halt nicht, deshalb gibt es eine Exception.
====


== Komplexe Typ-Hierarchie

Gegeben seien folgende Interfaces und Klassen. Die Namen sind bewusst so gewählt, dass man nicht ohne weiteres bestimmen kann, wie die Typen miteinander verwandt sind:

[source,java]
----
public interface Aleph {}
public class AlephImpl implements Aleph {}

public interface Beth extends Aleph {}
public class BethImpl implements Beth {}

public interface Gimel extends Aleph {}
public class GimelImpl implements Gimel {}

public interface Daleth extends Gimel {}
public class DalethImpl implements Daleth {}

public interface He extends Gimel {}
public class HeImpl implements He {}

public interface Taw {}
public class TawImpl implements Taw {}
----

Zeichnen Sie als Erstes die Typ-Hierarchie auf und bestimmen Sie dann wieder für jedes der folgenden Codestücke, in welche Kategorie es passt:

* fehlerfrei
* fehlerfrei, aber enthält einen unnötigen _Up-Cast_
* erzeugt einen Kompilierfehler
* wirft bei der Ausführung eine `ClassCastException`

[loweralpha]
. {empty}
+
[source,java]
----
Aleph a = new AlephImpl();
Beth b = a;
----

. {empty}
+
[source,java]
----
Gimel g = new GimelImpl();
Aleph a = g;
----

. {empty}
+
[source,java]
----
Beth b = new BethImpl();
Gimel g = b;
----

. {empty}
+
[source,java]
----
He h = new HeImpl();
Aleph a = h;
----

. {empty}
+
[source,java]
----
Gimel g = new DalethImpl();
Aleph a = g;
Daleth d = (Daleth) a;
----

. {empty}
+
[source,java]
----
Taw t = new TawImpl();
Aleph a = t;
----

. {empty}
+
[source,java]
----
Aleph a = new HeImpl();
Gimel g = (Gimel) a;
He h = (He) g;
----

. {empty}
+
[source,java]
----
Aleph a = (Aleph) new DalethImpl();
a = (Aleph) new GimelImpl();
a = (Aleph) new HeImpl();
----

. {empty}
+
[source,java]
----
Gimel g = (Gimel) new AlephImpl();
Aleph a = g;
----

. {empty}
+
[source,java]
----
Beth b = new BethImpl();
Aleph a = b;
b = (Beth) a;
----

.*Lösungen*
[%collapsible]
====
[.text-center]
image::res/aleph.png[width=500]

[loweralpha]
. Kompilierfehler
. fehlerfrei
. Kompilierfehler
. fehlerfrei
. fehlerfrei
. Komplilierfehler
. fehlerfrei
. unnötige Up-Casts
. `ClassCastException`
. fehlerfrei
====


== Overriding und `super`-Aufrufe

Gegeben seien folgende Interfaces und Klassen:

[source,java]
----
public interface Taschenmesser {
    String name();
    double preis();
}
----

[source,java]
----
public class Victorinox implements Taschenmesser {
    public String name() {
        return "Victorinox";
    }
    public double preis() {
        return 17.00;
    }
}
----

[source,java]
----
public class VictorinoxMitEtui extends Victorinox {
    public double preis() {
        return super.preis() + 13.00;
    }
}
----

[source,java]
----
public class Fisherman extends Victorinox {
    public String name() {
        return super.name() + " Fisherman";
    }
    public double preis() {
        return 37.00;
    }
}
----

[source,java]
----
public class NoNameMesser implements Taschenmesser {
    public String name() {
        return "No name";
    }
    public double preis() {
        return 5.95;
    }
}
----

Welche vier Zeilen gibt folgender Code aus?

[source,java]
----
Taschenmesser[] messer = {
        new Victorinox(),
        new VictorinoxMitEtui(),
        new Fisherman(),
        new NoNameMesser()};

for (Taschenmesser m : messer) {
    System.out.println(m.name() + ": " + m.preis());
}
----

.*Lösung*
[%collapsible]
====
----
Victorinox: 17.0
Victorinox: 30.0
Victorinox Fisherman: 37.0
No name: 5.95
----
====


== Nochmals `super`-Aufrufe

Gegeben sei folgende Klasse:

[source,java]
----
public class Student {
    private String name;
    private int age;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
----

Sie sollen nun eine Spezialisierung `BScStudent` schreiben, welche zusätzlich noch den Jahrgang speichert:

[source,java]
----
public class BScStudent extends Student {
    private int year;
    // ...
}
----

[loweralpha]
. Kann Code in der Klasse `BScStudent` auf die `name`- und `age`-Attribute zugreifen?
. Kann er `setAge` aufrufen?
. Schreiben Sie einen Konstruktor in `BScStudent`, welcher einen Namen entgegennimmt und ihn im Attribut `name` von `Student` speichert, `age` mit 18 initialisiert und `year` mit 0.
. Überschreiben Sie die `setAge`-Methode in `BScStudent`, sodass sie `age` (in `Student`) neu setzt und zusätzlich `year` um 1 erhöht.

.*Lösungen*
[%collapsible]
====
[loweralpha]
. Nein, `private` gilt auch gegenüber Subklassen.
. Ja, `setAge` ist `public` und kann deshalb von Subklassen mit `this.setAge(...)` oder auch einfach nur `setAge(...)` aufgerufen werden (solange die Subklasse die Methode nicht überschreibt – dann würde die überschriebene Methode aufgerufen).
. {empty}
+
[source,java]
----
public BScStudent(String name) {
    super(name, 18);
    year = 0;
}
----
. {empty}
+
[source,java]
----
public void setAge(int age) {
    super.setAge(age);
    year++;
}
----
====


== Eine Subklasse schreiben

Gegeben sei folgende Klasse:

[source,java]
----
public class Lamp {
    private boolean on;

    public Lamp(boolean on) {
        this.on = on;
    }

    public void switchOn() {
        on = true;
    }

    public void switchOff() {
        on = false;
    }
}
----

Stellen Sie aus folgenden Codezeilen eine Klasse `DimmableLamp` her, welche von `Lamp` erbt und zusätzlich noch die Helligkeit speichert. Verwenden Sie folgende Reihenfolge für die Elemente: Attribut, Konstruktor, Getter, Setter.

++++
<script>
    parsonsProblem`
        public class DimmableLamp extends Lamp {
            private int brightness;
            public DimmableLamp(boolean on, int brightness) {
                super(on);
                this.brightness = brightness;
            }
            public int getBrightness() {
                return brightness;
            }
            public void setBrightness(int brightness) {
                this.brightness = brightness;
            }
        }
        public class DimmableLamp { #distractor
        public class DimmableLamp implements Lamp { #distractor
        super(); #distractor
        public DimmableLamp(int brightness) { #distractor
    `;
</script>
++++

.*Hinweise zur Lösung*
[%collapsible]
====
Da die Superklasse `Lamp` nur _einen_ Konstruktor hat, mit einem `boolean`-Parameter, müssen die Konstruktoren einer Subklasse wie `DimmableLamp` diesen Konstruktor zwingend aufrufen, mit `super(...)`. Welche Argumente dabei übergeben werden, ist allerdings nicht vorgegeben. Man könnte in `DimmableLamp` z. B. auch folgenden Konstruktor schreiben:

[source,java]
----
public DimmableLamp(int brightness) {
    super(true);
    this.brightness = brightness;
}
----

Damit wären dimmbare Lampen nach dem Erstellen immer gleich eingeschaltet und man müsste beim Erstellen einer solchen Lampe nur noch die Helligkeit angeben:

[source,java]
----
DimmableLamp lamp = new DimmableLamp(50);
----
====


== Methoden überschreiben & `super`-Aufrufe

Gegeben sei folgende Klasse:

[source,java]
----
public class Bottle {
    private int capacity;  // ml
    private int content;   // ml

    public Bottle(int capacity) {
        if (capacity < 0) {
            throw new IllegalArgumentException();
        }
        this.capacity = capacity;
    }

    public void fill(int amount) {
        if (amount < 0 || content + amount > capacity) {
            throw new IllegalArgumentException();
        }
        content += amount;
    }

    public int getContent() {
        return content;
    }

    // weitere Methoden
}
----

Stellen Sie aus folgenden Codezeilen eine Klasse `SingleUseBottle` zusammen, welche von `Bottle` erbt, aber zwei zusätzliche Klassenregeln erzwingt:

. Die Kapazität ist fix 500 ml.
. Die Flasche kann nur einmal gefüllt werden, und zwar genau mit 500 ml.

++++
<script>
    parsonsProblem`
        public class SingleUseBottle extends Bottle {
            public SingleUseBottle() {
                super(500);
            }
            @Override
            public void fill(int amount) {
                if (amount != 500 || getContent() != 0) {
                    throw new IllegalArgumentException();
                }
                super.fill(500);
            }
        }
        public class SingleUseBottle { #distractor
        public SingleUseBottle(int capacity) { #distractor
        super(capacity); #distractor
        if (amount != 500 || content != 0) { #distractor
        if (amount != 500 || this.content != 0) { #distractor
        if (amount != 500 || super.content != 0) { #distractor
        fill(500); #distractor
        content += 500; #distractor
    `;
</script>
++++

.*Hinweise zur Lösung*
[%collapsible]
====
Da `content` in der Klasse `Bottle` die Sichtbarkeit `private` hat, kann man in `SingleUseBottle` nicht direkt darauf zugreifen, weder mit `content` noch mit `this.content` oder `super.content`. Stattdessen muss man den Getter `getContent()` verwenden, wie jeder andere Code ausserhalb der `Botte`-Klasse. (Nächste Woche werden wir sehen, wie Klassen ihren Subklassen Zugriff auf Attribute gewähren können.) `super.getContent()` würde auch funktionieren, ist aber unnötig, da `getContent()` in `SingleUseBottle` nicht überschrieben wurde; es wird also auch mit `getContent()` die Methode aus `Bottle` aufgerufen.

In der Methode `fill` muss aber zwingend `super.fill(...)` aufgerufen werden, sonst würde wieder die gleiche Methode in `SingleUseBottle` aufgerufen, was zu einer Endlosrekursion führen würde.
====


== Mehrfahrtenkarte

Um die Mechanismen von Vererbung nochmals zu üben, erweitern Sie das Ticket-Beispiel in Ihrem Code-Repository um eine weitere Subklasse `Mehrfahrtenkarte`.

Eine Mehrfahrtenkarte braucht keine weiteren Attribute, sondern gilt einfach für 10 Fahrten – man bezahlt aber nur 9. Die `preis`-Methode muss also überschrieben werden.

Wenn Sie den Code in `TicketShop` so erweitern:

[source,java]
----
Ticket[] tickets = new Ticket[6];

tickets[0] = new Ticket("Tellplatz", 2, false);
tickets[1] = new Ticket("Schützenhaus", 5, false);
tickets[2] = new Ticket("Basel SBB", 3, true);
tickets[3] = new Sparticket("Aesch Dorf", 2, false, 20);
tickets[4] = new Sparticket("Frick", 8, true, 15);
tickets[5] = new Mehrfahrtenkarte("Dreispitz", 2, false);
----

sollten Sie folgende Ausgabe erhalten:

----
Ihr Einkauf:
  Einzelticket 2. Klasse ab Tellplatz, 2 Zonen: CHF 5.30
  Einzelticket 2. Klasse ab Schützenhaus, 5 Zonen: CHF 9.80
  Einzelticket 1. Klasse ab Basel SBB, 3 Zonen: CHF 11.55
  Sparticket 2. Klasse ab Aesch Dorf, 2 Zonen: CHF 4.25 (-20%)
  Sparticket 1. Klasse ab Frick, 8 Zonen: CHF 20.65 (-15%)
  Mehrfahrtenkarte 10er 2. Klasse ab Dreispitz, 2 Zonen: CHF 47.70
Gesamtpreis: CHF 99.25
----


.*Lösung*
[%collapsible]
====
[source,java]
----
public class Mehrfahrtenkarte extends Ticket {

    public Mehrfahrtenkarte(String abfahrtsort, int zonen, boolean ersteKlasse) {
        super(abfahrtsort, zonen, ersteKlasse);
    }

    @Override
    public double preis() {
        return super.preis() * 9;
    }

    @Override
    public String toString() {
        return super.toString().replace("Einzelticket", "Mehrfahrtenkarte 10er");
    }
}
----
====
