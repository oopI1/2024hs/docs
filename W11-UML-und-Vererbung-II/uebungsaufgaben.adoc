= Übungsaufgaben
:sectnums:
:stem: latexmath
:icons: font
:hide-uri-scheme:

== Code lesen & Klassendiagramm (Vorlesung)

Lesen Sie die Klassen im Package `splitsmart` in Ihrem persönlichen Repository. Beginnen Sie bei `SplitSmartApp` und folgen Sie den Referenzen zu anderen Typen.

[loweralpha]
. Zeichnen Sie (von Hand) ein UML-Klassendiagramm, welches sämtliche Klassen, Beziehungen, Attribute & Methoden enthält. (Konstruktoren & Getter können Sie weglassen.)

. Vergleichen Sie mit der Person neben Ihnen. Diskutieren Sie Unterschiede und ergänzen Sie, wo nötig.

. Betrachten Sie die Klasse `Expense` genau. Fallen Ihnen Probleme/Unschönheiten auf?

.*Lösungen*
[%collapsible]
====
[loweralpha]
. image:res/splitsmart-uml.png[width=800]
+
(Generiert mit IntelliJ: Rechtsklick auf Package `splitsmart` → Diagrams → Show Diagram)
+
Beachten Sie, dass IntelliJ die Pfeile für die Vererbung mit ausgefüllten Spitzen zeichnet. Das ist nicht ganz korrekt, laut UML-Spezifikation sollten die Spitzen leer sein. Wichtiger ist allerdings die Unterscheidung zwischen Pfeilen mit Strichen als Spitze (Abhängigkeit/Assoziation/Komposition) und Pfeilen mit Dreiecken als Spitze (Vererbung/Implementation eines Interfaces). Diese Unterscheidung ist korrekt.

. Die Klasse `Expense` ist unvollständig implementiert; es fehlt eine sinnvolle Implementation der Methode `deptFor`, welche von Subklassen hinzugefügt wird. Die Dokumentation (Kommentare oberhalb der Klasse und der `deptFor`-Methode) weist darauf hin («_should_»), aber der Compiler kann das nicht überprüfen. Jemand, der die Dokumentation nicht liest, könnte deshalb mittels `new Expense(...)` sinnlose `Expense`-Objekte erstellen.
====


== `abstract`-Regeln (Vorlesung)

Welche der folgenden Klassen-Deklarationen sind gültig?

[loweralpha]
. {empty}
+
[source,java]
----
public abstract class Pet {
    private String name;
    public Pet(String name) {
        this.name = name;
    }
    public abstract void makeSound();
}
----

. {empty}
+
[source,java]
----
public abstract class Cat extends Pet {
    public Cat(String name) {
        super(name);
    }
}
----

. {empty}
+
[source,java]
----
public class Tiger extends Cat {}
----

. {empty}
+
[source,java]
----
public class Lion extends Cat {
    public Lion() { super("Leo"); }
}
----

. {empty}
+
[source,java]
----
public class Dog extends Pet {
    public Dog() { super("Bello"); }

    public void makeSound() {
        System.out.println("Woof!");
    }
}
----

. {empty}
+
[source,java]
----
public class Human {
    public abstract void talk();
}
----

.*Lösungen*
[%collapsible]
====
[loweralpha]
. gültig
. gültig: `Cat` erweitert zwar die abstrakte Klasse `Pet` und müsste grundsätzlich die abstrakte Methode `makeSound` implementieren. Da aber `Cat` selbst auch wieder `abstract` ist, muss die Methode nicht implementiert werden (sondern erst in den Subklassen von `Cat`).
. ungültig: `Tiger` erbt von `Cat` und muss deshalb einen Konstruktor haben, der den Superkonstruktor aufruft. Das gilt für abstrakte und normale (_konkrete_) Klassen. Ausserdem fehlt die Methode `makeSound` (siehe unten).
. ungültig: `Lion` erbt von der Klasse `Cat` und ruft richtigerweise den Superkonstruktor auf. Aber da `Cat` eine abstrakte Methode `makeSound` hat und `Lion` selbst nicht abstrakt ist, muss `Lion` die Methode implementieren.
. gültig
. ungültig: Nur abstrakte Klassen können abstrakte Methoden haben.
====


== `Expense` abstrakt (Vorlesung)

Machen Sie `Expense` zu einer abstrakten Klasse und deren Methode `deptFor` zu einer abstrakten Methode. Überprüfen Sie, z. B. in der `main`-Methode, dass es nicht mehr möglich ist, direkt `Expense`-Objekte zu erstellen.

.*Lösung*
[%collapsible]
====
[source,java]
----
package splitsmart;

/**
 * An expense with a description, an amount, and the person who paid.
 */
public abstract class Expense {

    private final String description;
    private final double amount;
    private final int paidBy;

    public Expense(String description, double amount, int paidBy) {
        this.description = description;
        this.amount = amount;
        this.paidBy = paidBy;
    }

    public String getDescription() {
        return description;
    }

    public double getAmount() {
        return amount;
    }

    public int getPaidBy() {
        return paidBy;
    }

    /**
     * Returns the amount that the given person owes for this expense. The
     * 'person' argument is the index of the person in the group. The returned
     * amount can be negative if that person has paid this expense, because then
     * the other people owe that person money.
     */
    public abstract double deptFor(int person, int numberOfPeople);

    @Override
    public String toString() {
        return String.format("%s: %.2f", getDescription(), amount);
    }
}
----
====


== `protected` (Vorlesung)

[loweralpha]
. Ändern Sie die Sichtbarkeit der Attribute von Expense auf protected und ändern Sie den Code in allen Subklassen so ab, dass nicht mehr die Getter, sondern direkt die Attribute verwendet werden.

. Kann man in der `main`-Methode der `SplitSmartApp` nun ebenfalls auf die Attribute zugreifen? Weshalb / weshalb nicht?

.*Lösungen*
[%collapsible]
====
[loweralpha]
. {empty}
+
[source,java]
----
package splitsmart;

/**
 * An expense with a description, an amount, and the person who paid.
 */
public abstract class Expense {

    protected final String description;
    protected final double amount;
    protected final int paidBy;

    public Expense(String description, double amount, int paidBy) {
        this.description = description;
        this.amount = amount;
        this.paidBy = paidBy;
    }

    public String getDescription() {
        return description;
    }

    public double getAmount() {
        return amount;
    }

    /**
     * Returns the amount that the given person owes for this expense. The
     * 'person' argument is the index of the person in the group. The returned
     * amount can be negative if that person has paid this expense, because then
     * the other people owe that person money.
     */
    public abstract double deptFor(int person, int numberOfPeople);

    @Override
    public String toString() {
        return String.format("%s: %.2f", getDescription(), amount);
    }
}
----

. Ja, denn `SplitSmartApp` befindet sich im selben Package wie `Expense`. Eine Klasse kann nicht Subklassen Zugriff geben, ohne dass auch alle anderen Klassen im gleichen Package Zugriff bekommen (in Java).
====


== `equals`-Methode (Vorlesung)

Gegeben sei folgende Klasse `Person`:

[source,java]
----
public class Person {
    private String name;
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    // TODO
}
----

Stellen Sie aus folgenden Codezeilen eine `equals`-Methode zusammen, welche man bei `// TODO` einfügen kann. Zwei Personen gelten als gleich, wenn sie den gleichen Namen und das gleiche Alter haben.

++++
<script>
    parsonsProblem`
        @Override
        public boolean equals(Object obj) {
            if (obj instanceof Person) {
                Person other = (Person) obj;
                return name.equals(other.name) && age == other.age;
            } else {
                return false;
            }
        }
        public boolean equals(Person other) { #distractor
        if (other instanceof Person) { #distractor
        return true; #distractor
        return name == other.name && age == other.age; #distractor
    `;
</script>
++++

.*Hinweis zur Lösung*
[%collapsible]
====
Wichtig ist, dass Attribute von `Person`, die keine primitiven Typen haben, auch wieder mit `equals` und nicht mit `==` verglichen werden. Deshalb wäre `name == other.name` falsch, da die Namen zwei unterschiedliche `String`-Objekte mit dem gleichen Inhalt sein könnten.
====


== UML-Klassendiagramm lesen

Gegeben sei folgendes Klassendiagramm:

image::res/esp.png[]

[loweralpha]
. Welche der folgenden Aussagen sind richtig?
+
--
. `Employee` erbt von `Person`.
. `Modulanlass` erbt von `Modul`.
. Zu jedem `Modulanlass` gehört genau ein `Modul`.
. Jedes `Modul` gehört genau zu einem `Modulanlass`.
. Ein Semester kann zu einem oder mehreren `Modulanlass` gehören, oder auch zu keinem.
. Jeder `Modulanlass` enthält mindestens eine studierende Person.
. Ein `Employee` hat drei Attribute.
. Eine `Person` hat nicht mehr als ein Attribut.
--

. Welche der folgenden Codestücke sind gültiger Client-Code für die Klassen in diesem Diagramm? Falls nicht, weshalb? (Für diese Aufgabe gehen wir davon aus, dass keine weiteren Methoden oder Konstruktoren in den Klassen vorhanden sind, als die, die im Diagramm gezeigt sind.)
+
--
. {empty}
+
[source,java]
----
Semester hs24 = new Semester(2024, true);
System.out.println(hs24.getYear());
----
. {empty}
+
[source,java]
----
Modul modul = new Modul("oopI1", 3);
Modulanlass anlass = new Modulanlass(modul, "HS24", "1iCa");
----
. {empty}
+
[source,java]
----
Employee e = new Employee("Alice", "5.2E56", "+41 61 123 45 67");
Person p = e;
System.out.println(p.getName());
----
. {empty}
+
[source,java]
----
Modul modul = new Modul("eipr", 3);
Modulanlass anlass = new Modulanlass(modul, new Semester(2024, true), "1iCa");
System.out.println(anlass.getName());
----
. {empty}
+
[source,java]
----
Semester semester = new Semester(2024, true);
if (semester.hs) {
    System.out.println("HS");
}
----
. {empty}
+
[source,java]
----
Semester hs24 = new Semester(2024, true);
Semester hs25 = new Semester(2025, true);
System.out.println(hs24.equals(hs25));
----
--

.*Lösungen*
[%collapsible]
====
[loweralpha]
. {empty}
+
--
. Richtig.
. Falsch: `Modulanlass` hat eine _Assoziation_ zur Klasse `Modul`, aber _erbt_ nicht von ihr (unterschiedliche Pfeilspitzen).
. Richtig.
. Falsch: Ein `Modul` kann in mehreren `Modulanlass`-Objekten vorkommen, denn die Multiplizität auf der Seite `Modulanlass` lautet «⁎», also «0 oder mehr».
. Richtig.
. Falsch: Es könnte auch ein `Modulanlass` ohne Studierende geben, denn die Multiplizität auf der Seite `Modulanlass` lautet «⁎», also «0 oder mehr».
. Richtig. (Eines wird geerbt von `Person`, zwei sind eigene Attribute.)
. Falsch: Wenn im Diagramm nur ein Attribut gezeigt wird, heisst das nicht, dass es nicht auch andere geben kann, sondern, dass für den Verwendungszweck dieses Diagramms nur dieses Attribut relevant ist.
--

. {empty}
+
--
. Ungültig: `Semester` hat keine Methode `getYear`. (Sie heisst `getJahr`.)
. Ungültig: Der Konstruktor von `Modulanlass` hat als zweiten Parameter einen vom Typ `Semester`, nicht `String`.
. Gültig.
. Ungültig: `Modulanlass` hat keine Methode `getName`. (Um den Namen des Moduls zu erhalten, müsste man `anlass.getModul().getName()` aufrufen.)
. Ungültig: Das Attribut `hs` ist ausserhalb der Klasse nicht sichtbar, da es `private` ist (dargestellt durch das Minuszeichen). Richtig wäre `semester.isHs()`.
. Gültig. (Jedes Objekt in Java hat die `equals`-Methode, da sie von der `Object`-Klasse geerbt wird.)
--
====


== Syntax von abstrakten Klassen

Gegeben sei folgende Klasse `Report`, welche von einer Klasse `Document` erbt:

[source,java]
----
public class Report extends Document {
    private int number;
    private String author;

    public Report(int year, int number, String author) {
        super(content, year);
        this.number = number;
        this.author = author;
    }

    // Getter

    @Override
    public String identifier() {
        return "Report " + number + " (" + year + ")";
    }
}
----

Stellen Sie aus folgenden Codezeilen eine abstrakte Superklasse `Document` mit einer abstrakten Methode `identifier` zusammen, sodass die Klasse `Report` kompiliert. Verwenden Sie folgende Reihenfolge: Attribut, Konstruktor, Getter, `identifier`-Methode.

++++
<script>
    parsonsProblem`
        public abstract class Document {
            protected int year;
            public Document(int year) {
                this.year = year;
            }
            public int getYear() {
                return year;
            }
            public abstract String identifier();
        }
        public class Document { #distractor
        private int year; #distractor
        public Document() { #distractor
        public abstract int getYear() { #distractor
        public abstract String identifier() {} #distractor
        public String identifier(); #distractor
        public String identifier() {} #distractor
    `;
</script>
++++

.*Hinweis zur Lösung*
[%collapsible]
====
Das `year`-Attribut muss `protected` sein, damit man in `Report` direkt (ohne Getter) darauf zugreifen kann. Die Default-Sicherbarkeit (_package-private_) würde auch gehen, wenn sich die beiden Klassen im selben Package befinden. In vielen Fällen ist es aber trotzdem empfehlenswert, Attribute `private` zu machen.
====


== `SinglePersonExpense`

Erstellen Sie eine neue Subklasse von `Expense` namens `SinglePersonExpense`, welche wie folgt funktioniert:

* Der Konstruktor hat alle Parameter vom Superkonstruktor, plus einen `int`-Parameter `coveredBy`, der angibt, welche Person die Kosten am Schluss tragen soll (`paidBy` gibt ja an, wer bisher bezahlt hat).
* Die Methode `deptFor` gibt `0` zurück, falls die Person, die bezahlt hat, auch die Kosten trägt, sonst je nach Person `amount`, `-amount` oder `0`. Überprüfen Sie Ihre Implementation mit dem vorgegebenen Unit-Test (der zu Beginn auskommentiert ist).

.*Lösung*
[%collapsible]
====
[source,java]
----
package splitsmart;

/**
 * An expense where the full amount is covered by a single person.
 */
public class SinglePersonExpense extends Expense {

    private final int coveredBy;

    public SinglePersonExpense(String description, double amount,
                               int paidBy, int coveredBy) {
        super(description, amount, paidBy);
        this.coveredBy = coveredBy;
    }

    @Override
    public double deptFor(int person, int numberOfPeople) {
        double dept = 0;
        if (person == coveredBy) {
            dept += amount;
        }
        if (person == paidBy) {
            dept -= amount;
        }
        return dept;
    }
}
----
====


== `equals` und Vererbung

*Fortgeschritten:* Bei der Diskussion zur `equals`-Methode haben wir bisher Vererbung zwischen eigenen Klassen ausser Acht gelassen. Für diese Übung gehen wir nochmals zum Beispiel mit den Tickets von der letzten Woche zurück.

[loweralpha]
. Überschreiben Sie als Erstes die `equals`-Methode in der Klasse `Ticket` im Aufgaben-Ordner von letzter Woche. Zwei Tickets gelten als gleich, wenn sie den gleichen Abfahrtsort, die gleiche Anzahl Zonen und die gleiche Klasse haben. Wir ignorieren vorerst weitere Ticket-Typen wie `Sparticket`.

. Sollte man in `Sparticket` die `equals`-Methode nochmals überschreiben?

. Welche Ausgabe würde man für folgendes Codestück erwarten?
+
[source,java]
----
Ticket t1 = new Ticket("Tellplatz", 2, false);
Ticket t2 = new Sparticket("Tellplatz", 2, false, 20);
System.out.println(t1.equals(t2));
System.out.println(t2.equals(t1));
----

. Versuchen Sie, `equals` in `Ticket` und `Sparticket` in der Vorlage von letzter Woche so zu implementieren, sodass die Implementationen den Erwartungen oben entsprechen. Hier könnte die Methode `getClass()` nützlich sein, die in der Vorlesung kurz erwähnt wurde. Sie brauchen nicht genau zu verstehen, was diese Methode zurückgibt; wichtig ist nur, dass es für Instanzen der gleichen Klasse das gleiche Objekt ist und für Instanzen von unterschiedlichen Klassen unterschiedliche Objekte.
+
Verwenden Sie den ersten Unit-Test in der Klasse `TicketEqualsTest` um Ihre Implementation zu überprüfen.

. Überprüfen Sie anhand der restlichen Unit-Tests in `TicketEqualsTest`, ob Ihre Implementation auch in Bezug auf Mehrfahrtenkarten korrekt ist. Nehmen Sie allenfalls noch Anpassungen vor (bzw. lösen Sie zuerst überhaupt mal die Übung zu Mehrfahrtenkarten von letzter Woche😉).

.*Lösungen*
[%collapsible]
====
[loweralpha]
. {empty}
+
[source,java]
----
@Override
public boolean equals(Object obj) {
    if (obj instanceof Ticket) {
        Ticket other = (Ticket) obj;
        return abfahrtsort.equals(other.abfahrtsort)
               && zonen == other.zonen
               && ersteKlasse == other.ersteKlasse;
    } else {
        return false;
    }
}
----

. Ja, denn sonst gelten `Sparticket`-Objekte mit unterschiedlichen Rabatten als gleich. Ebenfalls gelten `Ticket`-Objekte und `Sparticket`-Objekte als gleich, wenn sie die gleichen `Ticket`-Attribute haben.

. In beiden Fällen `false`, denn im Allgemeinen sollte es keine Rolle spielen, ob man `o1.equals(o2)` oder `o2.equals(o1)` aufruft.

. `equals`-Methode von `Ticket`:
+
[source,java]
----
@Override
public boolean equals(Object obj) {
    // Vergleich mit getClass() statt instanceof, damit Instanzen von
    // Subklassen sicher nicht als gleich wie eine Ticket-Instanz gelten
    if (obj != null && this.getClass() == obj.getClass()) {
        Ticket other = (Ticket) obj;
        return abfahrtsort.equals(other.abfahrtsort)
               && zonen == other.zonen
               && ersteKlasse == other.ersteKlasse;
    } else {
        return false;
    }
}
----
+
`equals`-Methode von `Sparticket`:
+
[source,java]
----
@Override
public boolean equals(Object obj) {
    if (obj instanceof Sparticket) {
        Sparticket other = (Sparticket) obj;
        return super.equals(other) && rabatt == other.rabatt;
    } else {
        return false;
    }
}
----

. Mit der Lösung für die `equals`-Methode von `Ticket` wie oben gezeigt, braucht `Mehrfahrtenkarte` keine eigene `equals`-Methode. Da Mehrfahrtenkarten keine zusätzlichen Attribute haben, ist die `equals`-Methode von `Ticket` auch für `Mehrfahrtenkarte` korrekt, da sie die genauen Typen vergleicht (mit `getClass()` statt `instanceof`).
====
