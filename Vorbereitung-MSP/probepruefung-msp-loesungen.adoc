= Probeprüfung (MSP) Lösungen

== Aufgabe 1

A: Kompiliert

B: Kompiliert

C: Kompiliert

D: Kompiliert

E: Kompiliert nicht, Fehler auf Zeile 2

F: Kompiliert nicht, Fehler auf Zeile 4

G: Kompiliert nicht, Fehler auf Zeile 1

H: Kompiliert nicht, Fehler auf Zeile 3



== Aufgabe 2

A: `MrX(1234): 98.0`

B: `MsY(22): 95.0`

C: `FHNW(456COM): 82.0`

D: `VIP Google(22000COM): 80.0`

E: `VIP Google(22000COM): 80.0`

== Aufgabe 3

A: `Person 1`

B: `true`

C: `false`

D: `250.0`

E: `Fehler`

